<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link https://www.capwelton.com})
 */


namespace Capwelton\App\Task\Ui;

use Capwelton\App\Task\Set\Task;


class TaskUi implements \app_ComponentUi
{
    private $app;
    private $ui;
    
    public function __construct(\Func_App $App){
        $this->app = $App;
        $this->ui = $App->Ui();
    }
    
    /**
     * @return TaskEditor
     */
    public function Editor($id = null, \Widget_Layout $layout = null)
    {
        return null;
    }
    
    /**
     *
     * @param Task $task
     *
     * @return TaskLinkedObjects
     */
    public function LinkedObjects(Task $task)
    {
        if(method_exists($this->ui, 'LinkedObjects')){
            return $this->ui->LinkedObjects($task);
        }
        return new TaskLinkedObjects($this->app, $task);
    }
    
    /**
     * @return TaskTableView
     */
    public function tableView($id = null)
    {
        if(method_exists($this->ui, 'TaskTableView')){
            return $this->ui->TaskTableView($id);
        }
        return new TaskTableView($this->app, $id);
    }
    
    /**
     * @return TaskTypeTableView
     */
    public function TypeTableView($id = null)
    {
        if(method_exists($this->ui, 'TypeTableView')){
            return $this->ui->TypeTableView($id);
        }
        return new TaskTypeTableView($this->app, $id);
    }
    
    public function UserTasks()
    {
        if(method_exists($this->ui, 'UserTasks')){
            return $this->ui->UserTasks();
        }
        return new UserTasks($this->app);
    }
    
    /**
     * @return TaskTypeEditor
     */
    public function typeEditor($id = null, \Widget_Layout $layout = null)
    {
        if(method_exists($this->ui, 'TaskTypeEditor')){
            return $this->ui->TaskTypeEditor($id, $layout);
        }
        return new TaskTypeEditor($this->app, $id, $layout);
    }
    
    /**
     * Task editor
     * @return TaskEditor
     */
    public function TaskEditor(Task $task, $reloaded = false, $id = null)
    {
        if(method_exists($this->ui, 'TaskEditor')){
            return $this->ui->TaskEditor($task, $reloaded, $id);
        }
        return new TaskEditor($this->app, $task, $reloaded, $id);
    }
    
    public function FullFrame(Task $task, $id = null)
    {
        if(method_exists($this->ui, 'TaskFullFrame')){
            $fullFrame = $this->ui->TaskFullFrame($task, $id);
        }
        else{
            $fullFrame = new TaskFullFrame($this->app, $id);
        }
        $fullFrame->setRecord($task);
        return $fullFrame;
    }
    
    public function SectionEditor(Task $task, $id = null, \Widget_Layout $layout = null)
    {
        if(method_exists($this->ui, 'TaskSectionEditor')){
            return $this->ui->TaskSectionEditor($task, $id, $layout);
        }
        return new TaskSectionEditor($task, $this->app, $id, $layout);
    }
    
    /**
     * Task card frame
     * @return TaskCardFrame
     */
    public function CardFrame(Task $task)
    {
        if(method_exists($this->ui, 'TaskCardFrame')){
            return $this->ui->TaskCardFrame($task);
        }
        return new TaskCardFrame($this->app, $task);
    }
    
    
    
    
    
    /**
     * @return SuggestParticipant
     */
    public function SuggestParticipant($task = null, $id = null)
    {
        $App = $this->app;
        $suggest = new SuggestParticipant($id);
        $suggest->setApp($App);
        $suggest->setTask($task);
        $suggest->setSuggestAction($App->Controller()->Task()->suggestParticipant($task), 'search');
        
        return $suggest;
    }
}