<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskType;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

class TaskCardFrame extends TaskFrame
{
    
    const SHOW_ALL = 0xFFFF;
    
    /**
     * @var int
     */
    protected $attributes;
    
    protected $section = null;
    protected $inlineMenu = null;
    protected $popupMenu = null;
    protected $toolbar = null;
    
    protected $taskController;
    protected $taskControllerNoProxy;
    
    static $now = null;
    
    
    /**
     * @param \Func_App $App
     * @param Task $task
     * @param int $attributes
     * @param string $id
     */
    public function __construct(\Func_App $App, Task $task, $attributes = TaskCardFrame::SHOW_ALL, $id = null)
    {
        $W = bab_Widgets();
        
        parent::__construct($App, $task, $id, $W->Layout());
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        
        if (!isset(self::$now)) {
            self::$now = \BAB_DateTime::now();
            self::$now = self::$now->getIsoDate();
        }
        
        $this->taskController = $App->Task()->Controller();
        $this->taskControllerNoProxy = $App->Task()->Controller(false);
        
        $this->attributes = $attributes;
         
        $this->content();
        $this->computeMenu();
    }
    
    public function content()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $task = $this->task;
        
        $content = $W->FlowItems(
            $headerBox = $W->FlowItems()->setSizePolicy('list-group-item row col-xs-12'),
            $bodyBox = $W->VBoxItems()->setSizePolicy('list-group-item row col-xs-12')->setVerticalSpacing(1, 'em'),
            $attendeesBox = $W->HBoxItems()->setSizePolicy('list-group-item row col-xs-12')->setHorizontalSpacing(1, 'em')
        )->addClass('list-group')->setIconFormat(16, 'left')->setSizePolicy('row');
        
        $checkboxLabel = $W->LabelledWidget(
            $task->summary,
            $checkbox = $W->CheckBox()
        );
        
        if($task->isUpdatable()) {
            $form = new \app_Editor($App, null, $W->FlowItems());
            $form->setName('data');
            $form->isAjax = true;
            $form->addClass('widget-no-close');
            $form->addItem(
                $W->Hidden(null, 'id', $task->id)
            );
            
            $checkbox->setName('completion');
            
            $form->addItem(
                $checkboxLabel
            );
            
            $form->setAjaxAction($this->taskController->changeTaskCompletionStatus(), '', 'change');
            $headerBox->addItem($form);
            
            $form->setSizePolicy('col-xs-10');
        }
        else{
            $checkbox->disable();
            $headerBox->addItem($checkboxLabel->setSizePolicy('col-xs-10'));
        }
        $checkbox->setValue($task->isCompleted());
        
        $menu = $W->Menu()->setSizePolicy('col-xs-2');
        $menu->setLayout($W->FlowLayout())->addClass(\Func_Icons::ICON_LEFT_16);
        $menu->setButtonLabel('&#9776;');
        $menu->setButtonClass('pull-right');
        $this->popupMenu = $menu;
        $headerBox->addItem($menu);
        
        $bodyBox->addItem(
            $this->taskControllerNoProxy->_dueDate($task->id)
        );
        
        $taskAttendees = $task->getAttendees();
        
        $Ui = $App->Ui();
        
        $contactCtrl = $App->Controller()->Contact();
        
        foreach ($taskAttendees as $taskAttendee) {
            $attendeeRecord = $App->getRecordByRef($taskAttendee->attendee);
            if (! ($attendeeRecord instanceof \Isi4You_Contact)) {
                continue;
            }
            $displayAction = $contactCtrl->display($attendeeRecord->id);
            $attendeesBox->addItem(
                $W->HBoxItems(
                    $W->Link(
                        $Ui->ContactAvatar($attendeeRecord, 20, 20)->setSizePolicy('minimum'),
                        $displayAction
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $W->Link(
                        $attendeeRecord->getFullName(),
                        $displayAction
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                )->setVerticalAlign('middle')
                ->setHorizontalSpacing(0.5, 'em')
            );  
        }
        
        $this->addItem($content);
    }
    
    public function computeMenu()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $task = $this->task;
        
        $isUpdtable = $task->isUpdatable();
        $isDeletable = $task->isDeletable();
        $isDuplicable = $task->isDuplicable();
        
        if (!empty($task->startedAction) && $task->completion != 100) {
            $this->addMenuItem(
                $W->Link(
                    $App->translate('Do this task'),
                    $task->startedAction
                ),
                \Func_Icons::ACTIONS_GO_NEXT
            );
            
        } else {
            if ($isUpdtable) {
                $this->addMenuItem(
                    $W->Link(
                        $App->translate('Edit this task...'),
                        $this->taskController->edit($task->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    \Func_Icons::ACTIONS_DOCUMENT_EDIT
                );
            }
        }
        
        $this->addMenuItem(
            $W->Link(
                $App->translate('View details'),
                $this->taskController->display($task->id)
            ),
            \Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        ); 
        
        
        
        if ($isDuplicable) {
            $this->addMenuItem(
                $W->Link(
                    $App->translate('Duplicate this task'),
                    $this->taskController->duplicate($task->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD),
                \Func_Icons::ACTIONS_EDIT_COPY
            );  
        }
        
        if ($isDeletable) {
            $this->addMenuItem(
                $W->Link(
                    $App->translate('Delete this task'),
                    $this->taskController->confirmDelete($task->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD),
                \Func_Icons::ACTIONS_EDIT_DELETE
            );
        }
        
        if ($isUpdtable && !$task->isPlanned()) {
            $this->addMenuItem(
                $W->Link(
                    $App->translate('Done'),
                    $this->taskController->markDone($task->id)
                )->setAjaxAction(null, $this->getId()),
                \Func_Icons::ACTIONS_DIALOG_OK
            );
        }
    }
    
    
    /**
     * @param \Widget_Displayable_Interface $item
     *
     * @return self
     */
    public function addLinkedItem($item)
    {
        $this->inlineMenu->addItem($item);
        
        return $this;
    }
    
    
    /**
     * @param \Widget_Displayable_Interface $item
     * @param bool $important
     *
     * @return self
     */
    public function addMenuItem(\Widget_Displayable_Interface $item, $classname = null, $important = true)
    {
        if (!isset($this->popupMenu)) {
            $this->popupMenu = $this->section->addContextMenu('popup');
            $this->popupMenu->addClass(\Func_Icons::ICON_LEFT_SYMBOLIC);
        }
        $this->popupMenu->addItem($item);
        
        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        
        return $this;
    }
    
    
    /**
     * @param \Widget_Displayable_Interface $item
     *
     * @return self
     */
    public function addToolbarItem(\Widget_Displayable_Interface $item, $classname = null)
    {
        $W = bab_Widgets();
        if (!isset($this->toolbar)) {
            $this->toolbar = $W->FlowLayout()
            ->setSpacing(4, 'px')
            ->addClass(\Func_Icons::ICON_LEFT_SYMBOLIC, 'app-small');
            $this->section->addItem($this->toolbar);
        }
        $this->toolbar->addItem($item->addClass('widget-actionbutton'));
        
        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        return $this;
    }
}