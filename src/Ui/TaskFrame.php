<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskSet;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @return TaskFrame
 */
class TaskFrame extends \app_CardFrame
{
    /**
     * @var Task
     */
    protected $task;
    
    /**
     * @var TaskSet
     */
    protected $set;
    
    public function __construct(\Func_App $App, Task $task, $id = null, $layout = null)
    {
        parent::__construct($App, $id, $layout);
        
        $this->task = $task;
        $this->set = $task->getParentSet();
    }
    
    
    
    protected function responsible()
    {
        if (!$this->task->responsible) {
            return null;
        }
        
        return $this->labeledField(
            $this->App()->translate('Responsible'),
            $this->task,
            $this->set->responsible
        );
    }
    
    
    protected function subTasks()
    {
        $App = $this->App();
        $W = bab_Widgets();
        $res = $this->task->selectChildTasks();
        
        if (0 === $res->count()) {
            return null;
        }
        
        $frame = $W->Frame(null,
            $W->VBoxLayout()
            ->setSpacing(.5, 'em')
        );
        
        $taskUi = $App->Task()->Ui();
        
        foreach($res as $subTask) {
            $card = $taskUi->TaskCardFrame($subTask);
            $frame->addItem($card);
        }
        
        return $W->VBoxItems(
            $W->Title($this->App()->translate('Sub-tasks')),
            $frame
        )->setVerticalSpacing(.5, 'em');
    }
    
    
    
    
    protected function getName()
    {
        $W = bab_Widgets();
        return $W->Title(implode(' / ', $this->task->getPathName()), 1);
    }
    
    protected function createdBy()
    {
        if (!$this->set->createdBy->isValueSet($this->task->createdBy)) {
            return null;
        }
        
        if ($this->task->createdBy === $this->task->responsible) {
            return null;
        }
        
        return $this->labelStr($this->App()->translate('Created by'), $this->set->createdBy->output($this->task->createdBy));
    }
    
    
    
    
    protected function description()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            $this->App()->translate('Description'),
            $W->Html($this->task->description)
        );
    }
    
    
    
    /**
     * duration in hours
     *
     *
     * @param string $label
     * @param \ORM_Field $field
     *
     * @return \Widget_Item
     */
    protected function durationField($label, \ORM_Field $field)
    {
        $fieldName = $field->getName();
        $method = 'getTree'.$fieldName;
        $value = $this->task->$method();
        
        $App = $this->App();
        
        if (!$field->isValueSet($value)) {
            return null;
        }
        
        $decimal = (float) $value;
        $displayable = $field->output($value);
        
        if (100 === 100 * $decimal) {
            $text = $App->translate('%s hour');
        } else {
            $text = $App->translate('%s hours');
        }
        
        return $this->labelStr($label, sprintf($text, $displayable));
    }
    
    
    
    protected function work()
    {
        return $this->durationField(
            $this->App()->translate('Planned work'),
            $this->set->work
        );
    }
    
    
    protected function actualWork()
    {
        return $this->durationField(
            $this->App()->translate('Actual work'),
            $this->set->actualWork
        );
    }
    
    
    protected function remainingWork()
    {
        return $this->durationField(
            $this->App()->translate('Remaining work'),
            $this->set->remainingWork
        );
    }
    
    
    
    protected function completion()
    {
        $W = bab_Widgets();
        
        $completion = $this->task->getTreeCompletion();
        
        if (!$completion) {
            return null;
        }
        
        return $W->Gauge()->setProgress($completion);
    }
    
    
    
    protected function startedAction()
    {
        if (!$this->set->startedAction->isValueSet($this->task->startedAction)) {
            return null;
        }
        
        if ($this->task->isCompleted()) {
            return null;
        }
        
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelStr(
            $App->translate('Do this task:'),
            $W->Link($App->translate('Access the task page'), $this->task->startedAction)
        );
    }
}