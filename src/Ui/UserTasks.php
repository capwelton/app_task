<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskSet;

bab_Widgets()->includePhpClass('widget_Frame');


/**
 *
 * extends Widget_VBoxLayout
 * @see Widget_VBoxLayout
 */
class UserTasks extends \app_UiObject
{
    /**
     * @var int
     */
    protected $userId = null;
    
    /**
     * @param \Func_App $app
     */
    public function __construct(\Func_App $app = null)
    {
        parent::__construct($app);

        // We simulate inheritance from Widget_VBoxLayout.
        $W = bab_Widgets();
        $this->setInheritedItem($W->VBoxLayout());
    }
    
    
    /**
     * @param Task $task
     * @return TaskCardFrame
     */
    public function taskPreview(Task $task)
    {
        $App = $this->App();
        
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        
        $taskPreview = $App->Task()->Ui()->TaskCardFrame($task);
        
        $linkSet = $App->LinkSet();
        $links = $linkSet->selectForTarget($task);
        
        foreach ($links as $link) {
            list(,$classname) = explode('_', $link->sourceClass);
            switch ($classname) {
                case 'Contact':
                    /**
                     * @todo Use Contact component
                     */
                    if ($contact = $App->ContactSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $App->Ui()->ContactPhoto($contact, 24, 24, false)
                        );
                    }
                    break;
                case 'Organization':
                    /**
                     * @todo Use Organization component
                     */
                    if ($org = $App->OrganizationSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $App->Ui()->OrganizationLogo($org, 24, 24, false)
                        );
                    }
                    break;
                case 'Deal':
                    /**
                     * @todo Use Deal component
                     */
                    if ($deal = $App->DealSet()->get($link->sourceId)) {
                        $taskPreview->addLinkedItem(
                            $App->Ui()->DealPhoto($deal, 24, 24, false, true)
                        );
                    }
                    break;
            }
        }
        
        return $taskPreview;
    }
    
    
    protected function section($name, $classname)
    {
        $W = bab_Widgets();
        $sectionTitleLevel = 3;
        $section = $W->Section(
            $name,
            $W->VBoxItems()->addClass($classname),
            $sectionTitleLevel
        );
        
        $section->addClass('compact')
        ->setPersistent(true)
        ->setFoldable(false);
        
        return $section;
    }
    
    /**
     * Defines the user
     *
     * @param int		$userId		The ovidentia user id
     * @return UserTasks
     */
    public function setUser($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    
    
    /**
     *
     * @param TaskSet $taskSet
     * @return Task[]
     */
    protected function selectTasks(TaskSet $taskSet)
    {
        $tasks = $taskSet->select(
            $taskSet->responsible->is($this->userId)
            ->_AND_($taskSet->isNotCompleted())
        );
        
        return $tasks;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\widget_Canvas $canvas)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $this->setVerticalSpacing(0.5, 'em');
        $this->addClass(\Func_Icons::ICON_LEFT_16);
        
        $taskSet = $App->TaskSet();
        
        $tasks = $this->selectTasks($taskSet);
        
        $tasks->orderAsc($taskSet->dueDate);
        
        $nbUnscheduledTasks = 0;
        $nbOverdueTasks = 0;
        $nbTodayTasks = 0;
        $nbThisWeekTasks = 0;
        $nbNextWeekTasks = 0;
        $nbLaterTasks = 0;
        
        $unscheduledTasks = $this->section($App->translate('Unscheduled'), 'crm-tasks-summary-unscheduled');
        
        $overdueTasks = $this->section($App->translate('Overdue'), 'crm-tasks-summary-overdue');
        
        $todayTasks = $this->section($App->translate('Today'), 'crm-tasks-summary-today');
        
        $thisWeekTasks = $this->section($App->translate('This week'), 'crm-tasks-summary');
        
        $nextWeekTasks = $this->section($App->translate('Next week'), 'crm-tasks-summary');
        
        $laterTasks = $this->section($App->translate('Later'), 'crm-tasks-summary');
        
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        $today = \BAB_DateTime::now();
        $endOfWeek = $today->cloneDate();
        $endOfWeek->add(1, BAB_DATETIME_DAY);
        while ($endOfWeek->getDayOfWeek() != 1) {
            $endOfWeek->add(1, BAB_DATETIME_DAY);
        }
        $endOfNextWeek = $endOfWeek->cloneDate();
        $endOfNextWeek->add(7, BAB_DATETIME_DAY);
        
        $todayIso = $today->getIsoDate();
        $endOfWeekIso = $endOfWeek->getIsoDate();
        $endOfNextWeekIso = $endOfNextWeek->getIsoDate();
        
        foreach ($tasks as $task) {
            /* @var $task Task  */
            if ($task->dueDate === '0000-00-00') {
                $nbUnscheduledTasks++;
                $taskFrame = $unscheduledTasks;
            } else if ($task->dueDate < $todayIso) {
                $nbOverdueTasks++;
                $taskFrame = $overdueTasks;
            } else if ($task->dueDate === $todayIso) {
                $nbTodayTasks++;
                $taskFrame = $todayTasks;
            } else if ($task->dueDate < $endOfWeekIso) {
                $nbThisWeekTasks++;
                $taskFrame = $thisWeekTasks;
            } else if ($task->dueDate < $endOfNextWeekIso) {
                $nbNextWeekTasks++;
                $taskFrame = $nextWeekTasks;
            } else {
                $nbLaterTasks++;
                $taskFrame = $laterTasks;
            }
            
            $taskFrame->addItem(
                $this->taskPreview($task)
                ->setSizePolicy('widget-list-element')
                );
        }
        
        if ($nbUnscheduledTasks > 0) {
            $m = $unscheduledTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbUnscheduledTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($unscheduledTasks);
        }
        if ($nbOverdueTasks > 0) {
            $m = $overdueTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbOverdueTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($overdueTasks);
        }
        if ($nbTodayTasks > 0) {
            $m = $todayTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbTodayTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($todayTasks);
        }
        if ($nbThisWeekTasks > 0) {
            $m = $thisWeekTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbThisWeekTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($thisWeekTasks);
        }
        if ($nbNextWeekTasks > 0) {
            $m = $nextWeekTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbNextWeekTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($nextWeekTasks);
        }
        if ($nbLaterTasks > 0) {
            $m = $laterTasks->addContextMenu();
            $m->addItem($W->Label((string)$nbLaterTasks)->addClass('widget-counter-label crm-counter-label crm-small'));
            $this->addItem($laterTasks);
        }
        
        return parent::display($canvas);
    }
}