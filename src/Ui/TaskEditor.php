<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @method Func_App App()
 * @return TaskEditor
 */
class TaskEditor extends \app_RecordEditor
{
    protected $task = null;
    protected $taskTypes = array();
    protected $reloaded = false;
    
    protected $taskTypeComponent = false;
    
    
    /**
     * @param \Func_App $app
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $app, \app_Record $task = null, $reloaded = false, $id = null, \Widget_Layout $layout = null)
    {
        $component = $app->getComponentByName('TaskType');
        if($component){
            $taskTypeSet = $component->recordSet();
            $this->taskTypes = $taskTypeSet->select();
            $this->taskTypeComponent = $component;
        }
        
        $this->task = $task;
        $this->reloaded = $reloaded;
        
        parent::__construct($app, $id, $layout);
        
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setSaveAction(
            $app->getComponentByName('Task')->controller()->save(),
            $app->translate('Add this task')
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \app_Editor::setValues()
     */
    public function setValues($record, $namePathBase = array())
    {
        if (! ($record instanceof \app_Record)) {
            $record['scheduledStartTime'] = substr($record['scheduledStart'], 11, 5);
            $record['scheduledFinishTime'] = substr($record['scheduledFinish'], 11, 5);
            
            return parent::setValues($record, $namePathBase);
        }
        
        $values = $record->getValues();
        
        $values['scheduledStartTime'] = substr($values['scheduledStart'], 11, 5);
        $values['scheduledFinishTime'] = substr($values['scheduledFinish'], 11, 5);
        
        parent::setValues($values, $namePathBase);
        
        return $this;
    }
    
    protected function appendButtons()
    {
        if(!$this->reloaded){
            $W = $this->widgets;
            $App = $this->App();
            
            if (isset($this->saveAction)) {
                $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $App->translate('Save');
                $submitButton = $W->SubmitButton();
                $submitButton->validate()
                ->setAction($this->saveAction)
                ->setFailedAction($this->failedAction)
                ->setSuccessAction($this->successAction)
                ->setLabel($saveLabel);
                if ($this->isAjax) {
                    $submitButton->setAjaxAction();
                }
                $this->addButton($submitButton);
            }
            
            if (isset($this->cancelAction)) {
                $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $App->translate('Cancel');
                $this->addButton(
                    $W->SubmitButton(/*'cancel'*/)
                    ->addClass('widget-close-dialog')
                    ->setAction($this->cancelAction)
                    ->setLabel($cancelLabel)
                );
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \app_Editor::prependFields()
     */
    protected function appendFields()
    {
        $W = $this->widgets;
        
        $App = $this->App();        
        
        $summaryFormItem = $this->labelledField(
            $App->translate('Title'),
            $W->LineEdit()->setSize(52)->setMaxSize(255)
            ->addClass('widget-fullwidth')
            ->setMandatory(true, $App->translate('The title must not be empty.')),
            'summary'
        );
        
        $this->addItem(
            $W->Flowitems(
                $this->_private(),
                $this->milestone()
            )->setHorizontalSpacing(2, 'em')
            ->setSizePolicy('pull-right ' . \Func_Icons::ICON_LEFT_16)
        );
        
        $this->addItem($W->Hidden()->setName('id'));
        
        $this->addItem($summaryFormItem);
        
        $this->addItem($this->type());
        
        $this->addItem($this->when());
        
        $this->addItem($this->attendees());
        
        $this->addItem($this->completion());
        
        $this->addItem($this->alarms());
        
        $this->TaskSections();
        
        $this->isAjax = true;
        $this->setHiddenValue('tg', $App->controllerTg);
    }
    
    protected function getBlueprint()
    {
        if($this->task && $type = $this->task->type()){
            return $type->getBlueprint();
        }
        
        return $this->taskTypeComponent->recordSet()->getDefaultBlueprint();
    }
    
    protected function TaskSections()
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();
        
        $customSections = $customSectionSet->select($customSectionSet->view->is($this->getBlueprint())->_AND_($customSectionSet->object->is('Task')));
        $customSections->orderAsc($customSectionSet->rank);
        
        $editor = new \app_RecordEditor($App);
        $editor->recordSet = $this->task->getParentSet();
        $editor->setRecord($this->task);
        
        foreach ($customSections as $customSection){
            $section = $editor->addSection($customSection->id, $customSection->name);
            $section->setFoldable($customSection->foldable, $customSection->folded);
            $section->addClass($customSection->classname);
            $section->setSizePolicy($customSection->sizePolicy);
            $this->addItem($section);
            
            $displayFields = $customSection->getFields();
            
            foreach ($displayFields as $displayField) {
                $widget = null;
                $item = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) && $parameters['label'] !== '__' ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;
                if (method_exists($this, $displayFieldMethod)) {
                    $widget = $this->$displayFieldMethod($customSection, $label);
                    $item = $widget;
                } elseif ($editor->recordSet->fieldExist($displayFieldName)) {
                    $field = $editor->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $App->translate($label);
                        }
                    }
                    $widget = $field->getWidget();
                    if ($widget instanceof \Widget_TextEdit || $widget instanceof \Widget_Select) {
                        $widget->addClass('widget-100pc');
                    }
                    $item = $editor->getValueItem($customSection, $displayField, $label, $widget);
                }
                
                if ($item) {
                    $item->addClass($classname);
                    $section->addItem($item);
                }
            }
        }
    }
    
    
    protected function _private()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->FlowItems(
            $W->Html($App->translate('Private')),
            $W->CheckBox()
            ->setName('private')
        )->setVerticalAlign('middle')
        ->setHorizontalSpacing(1, 'em')
        ->addClass('icon', \Func_Icons::ACTIONS_AUTHENTICATE);
    }
    
    protected function milestone()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->FlowItems(
            $W->Html($App->translate('Milestone')),
            $W->CheckBox()
            ->setName('milestone')
        )->setVerticalAlign('middle')
        ->setHorizontalSpacing(1, 'em')
        ->addClass('icon', 'objects-flag');
    }
    
    protected function type()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $taskTypeSet = $App->TaskTypeSet();
        $taskTypes = $taskTypeSet->select();
        
        
        $select = $W->Select();
        $select->addOption(0, '');
        
        foreach ($taskTypes as $taskType) {
            $select->addOption($taskType->id, $taskType->name);
        }
        
        return $this->labelledField(
            $App->translate('Type'),
            $select,
            'type'
        );
    }
    
    
    protected function when()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $dateItem = $W->DatePicker();
        $startTimeItem = $W->TimeEdit();
        $endTimeItem = $W->TimeEdit();
        
        $whenFormItem = $this->labelledField(
            $App->translate('When'),
            $W->FlowItems(
                $dateItem->setName('dueDate'),
                $startTimeItem->setName('scheduledStartTime'),
                $endTimeItem->setName('scheduledFinishTime')
            )->setHorizontalSpacing(2, 'em')
        );
        
        $sendFormItem = $this->labelledField(
            $App->translate('Add to my calendar'),
            $W->CheckBox()->setName('send')
        );
        
        return $W->VBoxItems(
            $whenFormItem,
            $sendFormItem
        );
    }
    
    protected function attendees()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $record = $this->getRecord();
        
        return $W->LabelledWidget(
            $App->translate('Attendees'),
            $W->VBoxItems(
                $App->Controller()->Task(false)->_attendees($record->id)
            )
        );
    }
    
    
    protected function alarms()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->LabelledWidget(
            $App->translate('Notifications'),
            $W->VBoxItems(
                $W->Link(
                    $App->translate('Add a notification'),
                    $App->Controller()->Alarm()->add($this->record->id)
                )->addClass('widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG),
            $App->Controller()->Alarm(false)->listFor($this->record->id)
            )
        );
    }
    
    
    protected function completion()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->LabelledWidget(
            $App->translate('Completion'),
            $W->NumberEdit()
            ->setStep(5)
            ->setMin(0)
            ->setMax(100)
            ->addAttribute('type', 'range'),
            'completion',
            null,
            '%'
        )->addClass('widget-10em');
    }
    
    public function _description($customSection)
    {
        $App = $this->App();
        $W = $this->widgets;
        $item = $W->CKEditor()
            ->setLines(12)
            ->setName('description')
            ->setColumns(70)
            ->addClass('widget-fullwidth');
            $item->setMandatory(true, $App->translate('The note must not be empty.'));
        
        return $this->labelledField($App->translate('Description'), $item);
    }
}