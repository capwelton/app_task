<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskSet;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

/**
 * @property Task $record
 * @return TaskFullFrame
 */
class TaskFullFrame extends \app_RecordView
{    
    /**
     * @var $set TaskSet
     */
    public $set;
    public function __construct(\Func_App $App, $id = null, $layout = null)
    {
        $this->set = $App->Task()->recordSet();
        parent::__construct($App, $id, $layout);
    }
    
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addInfoSection();
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    protected function getObjectName()
    {
        return 'Task';
    }
    
    protected function addInfoSection()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $row = $W->Items()->setSizePolicy('row');
        
        $row->addItems(
            $taskSection = $W->Section(
                $App->translate('Task'),
                $W->VBoxItems(
                    $this->_summary(),
                    $this->_type(),
                    $this->_when(),
                    $this->_completion()    
                )
            )->addClass('box')->setSizePolicy('col-md-12'),
            $W->Section(
                $App->translate('Attendees'),
                $W->VBoxItems(
                    $this->_attendees(),
                    $this->_alarms()
                )
            )->addClass('box blue')->setSizePolicy('col-md-12'),
            $W->VBoxItems(
                $App->Task()->Controller(false)->getLinkedElementsEditor($this->record->id)
            )->setSizePolicy('col-md-12')
        );
        
        $menu = $taskSection->addContextMenu('inline');
        $menu->addClass(\Func_Icons::ICON_LEFT_16);
        $menu->addItem(
            $W->Link(
                '',
                $this->record->getController()->editTaskSection($this->record->id)
            )->addClass('widget-actionbutton', 'section-button', 'icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        $this->addItem($row);
    }
    
    protected function _summary($customSection = null)
    {
        $W = bab_Widgets();
        return $W->Title($this->record->summary, 1);
    }
    
    protected function _type($customSection = null)
    {
        $W = bab_Widgets();
        if($this->record->type()){
            $type = $this->record->type()->name;
            return $W->LabelledWidget(
                $this->App()->translate('Type'),
                $W->Label($type)
            );
        }
        return null;
    }
    
    protected function _when($customSection = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $start = \BAB_DateTime::fromIsoDateTime($this->record->scheduledStart);
        $end = \BAB_DateTime::fromIsoDateTime($this->record->scheduledFinish);
        
        $startDate = $start->getIsoDate();
        $endDate = $end->getIsoDate();
        
        $startTime = $start->getIsoTime();
        $endTime = $end->getIsoTime();
        
        if($startDate == $endDate){
            if($startTime == $endTime){
                return $W->LabelledWidget(
                    $App->translate('When'),
                    $W->Label(bab_shortDate(bab_mktime($this->record->scheduledStart)))
                );
            }
            else{
                return $W->LabelledWidget(
                    $App->translate('When'),
                    $W->Label(sprintf($App->translate('%s from %s to %s'), bab_shortDate(bab_mktime($this->record->scheduledStart), false), $startTime, $endTime))
                );
            }
        }
    }
    
    protected function _attendees()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $W->LabelledWidget(
            $App->translate('Attendees'),
            $W->VBoxItems(
                $App->Task()->Controller(false)->_attendees($this->record->id)
            )
        );
    }
    
    protected function _alarms()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        return $W->LabelledWidget(
            $App->translate('Notifications'),
            $W->VBoxItems(
                $W->Link(
                    $App->translate('Add a notification'),
                    $App->Controller()->Alarm()->add($this->record->id)
                )->addClass('widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                $App->Controller()->Alarm(false)->listFor($this->record->id)
            )
        );
    }
    
    protected function _responsible($customSection = null)
    {
        if (!$this->record->responsible) {
            return null;
        }
        
        return $this->labelledWidget(
            $this->App()->translate('Responsible'),
            $this->record->responsible()->getFullName(),
            $customSection
        );
    }
    
    protected function _createdBy($customSection = null)
    {
        if (!$this->set->createdBy->isValueSet($this->task->createdBy)) {
            return null;
        }
        
        if ($this->task->createdBy === $this->task->responsible) {
            return null;
        }
        
        return $this->labelledWidget(
            $this->App()->translate('Created by'),
            $this->set->createdBy->output($this->record->createdBy),
            $customSection
        );
    }

    protected function _dueDate($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Due date'),
            $this->set->dueDate->output($this->record->dueDate),
            $customSection
        );
    }

    protected function _scheduledStart($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Scheduled start date'),
            $this->getScheduledDate('scheduledStart'),
            $customSection
        );
    }

    protected function _scheduledFinish($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Scheduled finish date'),
            $this->getScheduledDate('scheduledFinish'),
            $customSection
        );
    }
    
    protected function _description($customSection = null)
    {
        $W = bab_Widgets();
        return $W->labelledWidget(
            $this->App()->translate('Description'),
            $W->Html($this->record->description),
            $customSection
        );
    }
    
    protected function _durationQuantities($customSection = null)
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->_work(),
            $this->_actualWork(),
            $this->_remainingWork()
        )->setHorizontalSpacing(3, 'em');
    }
    
    protected function _completion($customSection = null)
    {
        $W = bab_Widgets();
        
        $completion = $this->record->getTreeCompletion();
        
        if (!$completion) {
            return null;
        }
        
        return $this->labelledWidget(
            $this->App()->translate('Completion'),
            $W->Gauge()->setProgress($completion),
            $customSection
        );
    }
    
    protected function _startedAction($customSection = null)
    {
        if (!$this->set->startedAction->isValueSet($this->record->startedAction)) {
            return null;
        }
        
        if ($this->record->isCompleted()) {
            return null;
        }
        
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            $this->App()->translate('Do this task'),
            $W->Link($App->translate('Access the task page'), $this->record->startedAction),
            $customSection
        );
    }
    
    protected function _subTasks($customSection = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $res = $this->record->selectChildTasks();
        
        if (0 === $res->count()) {
            return null;
        }
        
        $frame = $W->Frame(null,
            $W->VBoxLayout()
            ->setSpacing(.5, 'em')
        );
        
        $taskUi = $App->Task()->Ui();
        
        foreach($res as $subTask) {
            $card = $taskUi->TaskCardFrame($subTask);
            $frame->addItem($card);
        }
        
        return $this->labelledWidget(
            $App->translate('Sub-tasks'),
            $frame,
            $customSection
        );
    }
    
    protected function _work($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Planned work'),
            $this->getDurationField($this->set->work),
            $customSection
        );
    }
    
    protected function _actualWork($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Planned work'),
            $this->getDurationField($this->set->actualWork),
            $customSection
        );
    }
    
    protected function _remainingWork($customSection = null)
    {
        return $this->labelledWidget(
            $this->App()->translate('Planned work'),
            $this->getDurationField($this->set->remainingWork),
            $customSection
        );
    }
    
    
    private function getScheduledDate($name)
    {
        $value = $this->record->$name;
        $field = $this->set->$name;
        
        if (!$field->isValueSet($value)) {
            return null;
        }
        
        $displayable = $field->output($value);
        
        if ($this->task->scheduledAllDay) {
            list($displayable) = explode(' ', $displayable);
        }
        
        return $displayable;
    }
    
    private function getDurationField(\ORM_Field $field)
    {
        $fieldName = $field->getName();
        $method = 'getTree'.$fieldName;
        $value = $this->record->$method();
        
        $App = $this->App();
        
        if (!$field->isValueSet($value)) {
            return null;
        }
        
        $decimal = (float) $value;
        $displayable = $field->output($value);
        
        if (100 === 100 * $decimal) {
            $text = $App->translate('%s hour');
        } else {
            $text = $App->translate('%s hours');
        }
        
        $W = bab_Widgets();
        return $W->Label(sprintf($text, $displayable));
    }
}