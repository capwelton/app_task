<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;

include_once 'base.php';
$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');




/**
 * A SuggestParticipant
 */
class SuggestParticipant extends \Widget_SuggestLineEdit implements \Widget_Displayable_Interface, \app_Object_Interface
{
    private $app = null;
    
    private $task = null;
    
    protected $criteria = null;
    
    
    /**
     * Get App object
     * @return \Func_App
     */
    public function App()
    {
        return $this->app;
    }
    
    
    /**
     * Forces the \Func_App object to which this object is 'linked'.
     *
     * @param \Func_App	$app
     * @return self
     */
    public function setApp(\Func_App $app = null)
    {
        $this->app = $app;
        return $this;
    }
    
    
    
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }
    
    /**
     * Specifies criteria that will be applied to suggested contacts.
     *
     * @return self
     */
    public function setCriteria(\ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }
    
    
    
    /**
     * Send suggestions
     */
    public function suggest()
    {
        
        if (false !== $keyword = $this->getSearchKeyword()) {
            
            $suggestions = array();
            
            $App = $this->App();
            $contactSet = $App->ContactSet();
            $contactSet->mainPosition();
            $contactSet->mainPosition->organization();
            
            $criteria = $contactSet->lastname->startsWith($keyword);
            $criteria = $criteria->_OR_($contactSet->firstname->startsWith($keyword));
            $criteria = $criteria->_OR_(
                $contactSet->firstname->concat(' ', $contactSet->lastname)->startsWith($keyword)
            );
            
            if (isset($this->criteria)) {
                $criteria = $criteria->_AND_($this->criteria);
            }
            
            $i = 0;
            
            //             if (isset($this->task)) {
            //                 $taskSet = $App->TaskSet();
            // //                 $taskSet->setAllowedStatuses(null);
            //                 $task = $taskSet->request($this->task);
            
            //                 $teamSet = $App->TeamSet();
            //                 $teamSet->deal();
            
            //                 $teams = $teamSet->select($teamSet->deal->isSourceOf($task));
            
            //                 foreach ($teams as $team) {
            //                     $i++;
            //                     if ($i > Widget_SuggestLineEdit::MAX) {
            //                         break;
            //                     }
            
            //                     $suggestions[] = array(
            //                         'id' => $team->getRef(),
            //                         'value' => '[' . $App->translate('Team') . ']: ' . $team->name,
            //                         'info' => '',
            //                         'extra' => ''
            //                     );}
            
            //             }
            
            $contacts = $contactSet->select($criteria);
            $contacts->orderAsc($contactSet->lastname);
            
            foreach ($contacts as $contact) {
                $i++;
                if ($i > \Widget_SuggestLineEdit::MAX) {
                    break;
                }
                
                $organization = $contact->mainPosition->organization();
                $organizationName = $organization ? $organization->name : '';
                
                $suggestions[] = array(
                    'id' => $contact->getRef(),
                    'value' => $contact->getFullName(),
                    'info' => $organizationName,
                    'extra' => $organizationName
                );
            }
            
            
            
            \bab_Sort::asort($suggestions, 'value', \bab_Sort::CASE_INSENSITIVE);
            
            foreach ($suggestions as $suggestion) {
                $this->addSuggestion(
                    $suggestion['id'],
                    $suggestion['value'],
                    $suggestion['info'],
                    $suggestion['extra']
                );
            }
            
            $this->sendSuggestions();
        }
    }
    
    /**
     * Sets the value.
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($value instanceof \app_Record) {
            parent::setValue($value->getFullName());
            parent::setIdValue($value->getRef());
        } else {
            parent::setValue($value);
        }
        return $this;
    }
    
    
    /**
     * (non-PHPdoc)
     * @see \Widget_SuggestLineEdit::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }
}