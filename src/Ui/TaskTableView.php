<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskSet;

bab_Widgets()->includePhpClass('widget_TableView');

/**
 * @method TaskSet getRecordSet()
 */
class TaskTableView extends \app_TableModelView
{
    public $sectionCtrl;
    public $taskCtrl;
    
    public function __construct(\Func_App $app = null, $id = null){
        
        $component = $app->getComponentByName('Task');
        if(!$component){
            throw new \app_Exception('The Task component has not been found');
        }
        
        $this->taskCtrl = $component->controller();
        
        parent::__construct($app, $id);
        
        $this->addClass('depends-Task');
    }
    
    public function addDefaultColumns(TaskSet $set)
    {
        $App =  $this->App();
        
        $set->type();
        
        $this->setIconFormat(16, 'left');
        $this->addColumn(widget_TableModelViewColumn($set->summary, $App->translate('Title'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->description, $App->translate('Description'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->type()->name, $App->translate('Type'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->responsible, $App->translate('Responsible'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->scheduledStart, $App->translate('Scheduled start'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->scheduledFinish, $App->translate('Scheduled finish'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->completion, $App->translate('Completion'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->work, $App->translate('Work'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->actualWork, $App->translate('Actual work'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->remainingWork, $App->translate('Remaining work'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->startedOn, $App->translate('Started on'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->completedOn, $App->translate('Completed on'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->canceledOn, $App->translate('Canceled on'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn($set->attendees, $App->translate('Attendees'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn('_object_', $App->translate('Object')));
    }
    
    protected function computeCellContent(Task $record, $fieldPath)
    {
        $W = bab_Widgets();
        
        switch($fieldPath){
            case 'summary':
                $cellContent = $W->Link(
                    $record->summary,
                    $this->taskCtrl->display($record->id, $record->getBlueprint())
                );
                break;
            case 'responsible':
                $cellContent = $W->Label('');
                $responsible = $record->responsible();
                if (isset($responsible)) {
                    $cellContent->setText($responsible->getFullNameWithTitle());
                }
                break;
            case 'completion':
                $cellContent = $W->HBoxItems();
                if($record->isCompleted()){
                    //Completed
                    $badge = $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#00A65A')
                    )->setSizePolicy(\Widget_SizePolicy::MINIMUM)->addClass('crm-color-preview');
                }
                elseif($record->isLate()){
                    //Scheduled finish is in the past and task is not completed
                    $badge = $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#E95A47')
                    )->setSizePolicy(\Widget_SizePolicy::MINIMUM)->addClass('crm-color-preview');
                }
                else{
                    $badge = $W->Frame()->setCanvasOptions(
                        \Widget_Item::Options()->backgroundColor('#D2D6DE')
                    )->setSizePolicy(\Widget_SizePolicy::MINIMUM)->addClass('crm-color-preview');
                }
                $cellContent->addItems(
                    $badge,
                    $W->Label(bab_nbsp())->addClass('spacer'),
                    $W->Label(sprintf('%d%%', $record->completion))
                );
                break;
            case '_object_':
                $box = $W->VBoxItems();
                $objects = $record->getLinkedObjects();
                foreach ($objects as $object) {
                    /* @var $object \app_Record */
                    if($object){//This test is needed in case the object has been deleted (will be null)
                        $ctrl = $object->getController();
                        if(method_exists($ctrl, 'display')){
                            $box->addItem(
                                $W->Link($object->getRecordTitle(), $object->getController()->display($object->id))
                            );
                        }
                    }                 
                }
                return $box;
            default:
                $cellContent = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $cellContent;
    }
    
    protected function computeCellTextContent(Task $record, $fieldPath)
    {
        switch ($fieldPath){
            case '_object_':
                $values = array();
                $objects = $record->getLinkedObjects();
                foreach ($objects as $object) {
                    /* @var $object \app_Record */
                    $values[] = $object->getRecordTitle();
                }
                return implode("\n", $values);
                break;
            default:
                return parent::computeCellTextContent($record, $fieldPath);
                break;
        }
    }
    
    
    /**
     * {@inheritDoc}
     * @see app_TableModelView::handleFilterInputWidget()
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        $W = bab_Widgets();
        switch ($name) {
            case 'completion_state':
                $filter = $W->Select();
                $states = array(
                    "created" => 'created',
                    "started" => 'started',
                    "finished"=> 'finished'
                );
                $filter = $W->Multiselect()->setOptions($states);
                break;
            case 'type/name':
                $App = $this->App();
                $taskTypes = $App->taskTypeSet()->select();
                
                $options = array(0 => '');
                foreach ($taskTypes as $taskType){
                    $options[$taskType->id] = $taskType->name;
                }
                $filter = $W->Multiselect()->setOptions($options);
                break;
            case 'responsible':
                $filter = $this->App()->Ui()->SuggestContact();
                $filter->setMinChars(0);
                break;
            case 'attendees':
                $filter = $this->App()->Ui()->SuggestContact();
                $filter->setMinChars(0);
                break;
            default:
                $filter = parent::handleFilterInputWidget($name, $field);
                break;
        }
        return $filter;
    }
    
    
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::getFilterCriteria()
     */
    public function getFilterCriteria($filter = null)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();
        $conditions = array();
        
        if($filter["completion_state"]&& !empty($filter["completion_state"])){
            if(in_array("started",$filter["completion_state"])){
                if(in_array("created",$filter["completion_state"])){
                    $conditions[] = $recordSet->completion->isNot(100);
                }else if(in_array("finished",$filter["completion_state"])){
                    $conditions[] = $recordSet->completion->isNot(0);
                }else{
                    $conditions[] = $recordSet->completion->isNot(100)->_AND_($recordSet->completion->isNot(0));
                }
            }else{
                foreach($filter["completion_state"] as $val){
                    switch($val){
                        case("created"):
                            $conditions[] = $recordSet->completion->is(0);
                            break;
                        
                        case("finished"):
                            $conditions[] = $recordSet->completion->is(100);
                            break;
                    }  
                }
            }
        }unset($filter['completion_state']);
        
        if(isset($filter['search']) && !empty($filter['search'])){
            $conditions[] = $recordSet->summary->contains($filter['search']);
        }
        unset($filter['search']);
        
        if(isset($filter['responsible']) && !empty($filter['responsible'])){
            $conditions[] = $recordSet->responsible->is($filter['responsible']);
        }
        unset($filter['responsible']);
        
        if(isset($filter['attendees']) && !empty($filter['attendees'])){
            $contactSet = $App->ContactSet();
            $contact = $contactSet->get($filter['attendees']);
            if($contact){
                $conditions[] = $recordSet->hasAttendee($contact);
            }
        }
        unset($filter['attendees']);
        
        if (isset($filter['type/name']) && !empty($filter['type/name'])){
            $conditions[] = $recordSet->type->in($filter['type/name']);
        }
        unset($filter['type/name']);
        
        if(isset($filter['linkedTo'])){
            $App = $this->App();
            $source = $App->getRecordByRef($filter['linkedTo']);
            if($source){
                $subCondition = $recordSet->none();
                $links = $recordSet->selectLinkedTo($source);
                $ids = array();
                foreach ($links as $link){
                    $ids[] = $link->targetId->id;
                }
                if(count($ids)>0){
                    $subCondition = $subCondition->_OR_($recordSet->id->in($ids));
                }
                if($source->getClassName() == $App->ContactClassName()){
                    if($source->user != 0){
                        $subCondition = $subCondition->_OR_($recordSet->responsibleUser->is($source->user));
                    }
                    $subCondition = $subCondition->_OR_($recordSet->responsible->is($source->id));
                }
            }
            $conditions[] = $subCondition;
        }
        unset($filter['linkedTo']);
        
        $conditions[] = parent::getFilterCriteria($filter);
        return $recordSet->all($conditions);
    }
}



