<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\TaskTypeSet;

bab_Widgets()->includePhpClass('widget_TableView');


class TaskTypeTableView extends \app_TableModelView
{
    public $sectionCtrl;
    public $taskCtrl;
    
    public function __construct(\Func_App $app = null, $id = null){
        
        $component = $app->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        
        $this->sectionCtrl = $app->Controller()->CustomSection();
        $this->taskCtrl = $component->controller();
        
        parent::__construct($app, $id);
    }
    
    public function addDefaultColumns(TaskTypeSet $set)
    {
        $App =  $this->App();
        
        $this->setIconFormat(16, 'left');
        $this->addColumn(widget_TableModelViewColumn($set->name, $App->translate('Name'))->setExportable(true)->setSearchable(true));
        $this->addColumn(widget_TableModelViewColumn('_blueprint_', $App->translate('Blueprint'))->setExportable(false)->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->isDefault, $App->translate('Default type'))->setExportable(true)->setSearchable(true));
    }
    
    protected function computeCellContent($record, $fieldPath)
    {
        $W = bab_Widgets();
        
        switch($fieldPath){
            case 'name':
                $cellContent = $W->HBoxItems(
                    $W->Link('', $this->taskCtrl->comfirmDeleteTaskType($record->id))->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE),
                    $W->Link($record->name, $this->taskCtrl->editType($record->id))->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
                break;
            case '_blueprint_':
                $cellContent = $W->Link(
                    $this->App()->translate('Edit blueprint'),
                    $this->sectionCtrl->editSections('Task', $record->getBlueprint())
                )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
                break;
            default:
                $cellContent = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $cellContent;
    }
}



