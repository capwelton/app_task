<?php
namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\TaskType;





$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');





/**
 * @return TaskTypeEditor
 */
class TaskTypeEditor extends \app_Editor
{
    protected $controller;
    protected $taskType = null;


    /**
     * @param \Func_App $app
     * @param string $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $app, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($app, $id, $layout);
        
        $component = $app->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        
        $this->controller = $component->controller();
        
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setSaveAction(
            $this->controller->saveType(),
            $app->translate('Add this task type')
        );
    }


    /**
     * Add fields into form
     */
    public function prependFields($withOptions = true, $withAttachements = true)
    {
        $W = $this->widgets;

        $this->addItem(
            $W->VBoxItems(
                $W->LabelledWidget(
                    $this->App()->translate('Type'),
                    $W->LineEdit()->setMandatory(true, $this->App()->translate('The task type must not be empty.')),
                    'name'
                )->addClass('widget-fullwidth'),
                $W->LabelledWidget(
                    $this->App()->translate('Default type'),
                    $W->CheckBox(),
                    'isDefault'
                )->addClass('widget-fullwidth')
            )
        );
        
        return $this;
    }

    public function setTaskType(TaskType $taskType = null)
    {
        if (isset($taskType)) {
            $this->setRecord($taskType);
            $App = $this->App();
            
            $this->taskType = $taskType;
            $typeValues = $taskType->getValues();
            $this->setValues(array('taskType' => $typeValues));

            if (!empty($taskType->id)) {
                $this->setHiddenValue('data[id]', $taskType->id);
            }
            
            $this->setSaveAction($this->controller->saveType(), $App->translate('Save this task type'));
        }
    }

    /**
     * set editor in create task type mode
     */
    public function addTaskType()
    {
        $this->setTaskType();
    }
}

