<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;

use Capwelton\App\Task\Set\Task;

$W = bab_Widgets();
$W->includePhpClass('Widget_Frame');

class TaskSectionEditor extends \app_RecordEditor
{
    public function __construct(Task $task, \Func_App $app, $id = null, $layout = null)
    {
        parent::__construct($app, $id, $layout);   
        $this->setRecord($task);
    }
    
    /**
     * @return \Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        
        $box = $W->HBoxItems();
        
        if(!isset($this->itemId)){
            $box->setSizePolicy('widget-form-buttons')
            ->setHorizontalSpacing(1, 'em');
        }
        
        return $box;
    }
    
    protected function _description($customSection)
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Description'),
            $W->CKEditor(),
            'description'
        );
    }
    
    protected function _summary($customSection)
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Title'),
            $W->LineEdit(),
            'summary'
        );
    }
    
    protected function _type($customSection)
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $taskTypeSet = $App->TaskTypeSet();
        $taskTypes = $taskTypeSet->select();
        
        
        $select = $W->Select();
        $select->addOption(0, '');
        
        foreach ($taskTypes as $taskType) {
            $select->addOption($taskType->id, $taskType->name);
        }
        
        return $this->labelledField(
            $App->translate('Type'),
            $select,
            'type'
        );
    }
    
    protected function _dueDate($customSection)
    {
        $W = $this->widgets;
        $App = $this->App();
        
        $dateItem = $W->DatePicker();
        $startTimeItem = $W->TimeEdit();
        $endTimeItem = $W->TimeEdit();
        
        $whenFormItem = $this->labelledField(
            $App->translate('When'),
            $W->FlowItems(
                $dateItem->setName('dueDate'),
                $startTimeItem->setName('scheduledStartTime'),
                $endTimeItem->setName('scheduledFinishTime')
            )->setHorizontalSpacing(2, 'em')
        );
        
        $sendFormItem = $this->labelledField(
            $App->translate('Add to my calendar'),
            $W->CheckBox()->setName('send')
        );
        
        return $this->labelledWidget(
            $App->translate('When'),
            $W->VBoxItems(
                $whenFormItem,
                $sendFormItem
            ),
            $customSection
        );
    }
    
    protected function _attendees()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->LabelledWidget(
            $App->translate('Attendees'),
            $W->VBoxItems(
                $App->Controller()->Task(false)->_attendees($this->record->id)
            )
        );
    }
    
    protected function _completion()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->LabelledWidget(
            $App->translate('Completion'),
            $W->NumberEdit()
                ->setStep(5)
                ->setMin(0)
                ->setMax(100)
                ->addAttribute('type', 'range'),
            'completion',
            null,
            '%'
        )->addClass('widget-10em');
    }
    
    protected function _alarms()
    {
        $W = $this->widgets;
        $App = $this->App();
        
        return $W->LabelledWidget(
            $App->translate('Notifications'),
            $W->VBoxItems(
                $W->Link(
                    $App->translate('Add a notification'),
                    $App->Controller()->Alarm()->add($this->record->id)
                )->addClass('widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                $App->Controller()->Alarm(false)->listFor($this->record->id)
            )
        );
    }
}