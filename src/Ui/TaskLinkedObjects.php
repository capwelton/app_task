<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ui;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Set\TaskSet;

bab_Widgets()->includePhpClass('widget_Frame');


class TaskLinkedObjects extends \widget_Frame
{
    protected $App;
    protected $task;
    
    public function __construct(\Func_App $App, Task $task, $id = null, $layout = null)
    {
        $this->App = $App;
        $this->task = $task;
        parent::__construct($id, $layout);
        
        $this->prependFields();
    }
    
    public function App()
    {
        return $this->App;    
    }
    
    public function prependFields()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        /**
         * @todo Use Organization component
         */
        $App->includeOrganizationSet();
        
        $objectsFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        
        $objectsFrame->addItem(
            $this->ancestor()
        );
        $objectsFrame->addItem(
            $this->contacts()
        );
        
        $orgs = array();
        
        $objectsFrame->addItem(
            $this->deals($orgs)
        );
        $objectsFrame->addItem(
            $this->organizations($orgs)
        );
        
        $this->addItem($objectsFrame);
    }
    
    public function ancestor()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->TaskUi();
        
        $parent = $this->task->getScalarValue('parent');
        
        if (!$parent) {
            return null;
        }
        
        $frame = $W->Frame();
        $parentTask = $this->task->parent();
        
        $frame->addItem(
            $W->Title($App->translate('Set of tasks'), 4)->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
        );
        
        $frame->addItem($Ui->TaskCardFrame($parentTask));
        
        return $frame;
    }
    
    public function contacts()
    {
        /**
         * @todo Use Contact component
         */
        
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $contacts = $this->task->getLinkedContacts();
        
        
        if (0 === count($contacts)) {
            return null;
        }
        
        $frame = $W->Frame();
        
        $frame->addItem(
            $W->Title($App->translate('Contacts linked to this task'), 4)->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
        );
        
        foreach ($contacts as $contact) {
            $contactFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('app-card');
            $frame->addItem($contactFrame);
            $card = $Ui->contactCardFrame($contact);
            $contactFrame->addItem($card);
        }
        
        return $frame;
    }
    
    public function deals(&$orgs)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $dealComponent = $App->getComponentByName('Deal');
        if(!$dealComponent){
            return null;
        }
        
        
        $deals = $this->task->getLinkedDeals();
        if (0 === count($deals) > 0) {
            return null;
        }
        
        $frame = $W->Frame();
        
        $frame->addItem(
            $W->Title($App->translate('Deals linked to this task'), 4)
            ->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
        );
        
        $Ui = $App->Ui();
        
        foreach ($deals as $deal) {
            $card = $Ui->DealCardFrame($deal);
            $dealFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('app-card');
            $frame->addItem($dealFrame);
            
            $dealFrame->addItem($card);
            
            
            $contact = $deal->referralContact();
            
            $contactsLayout = $W->VBoxLayout()->setVerticalSpacing(2, 'px');
            
            if ($contact) {
                
                $contactsLayout->addItem($W->Label($App->translate('Referral contact'))->addClass('app-display-label '));
                
                $contactCard = $Ui->ContactCardFrame(
                    $contact
                );
                
                $contactSection = $W->Section(
                    $contact->getFullName(),
                    $contactCard,
                    6
                )->setFoldable(true, true);
                
                $contactSection->setSubHeaderText($contact->getMainPosition());
                $contactSection->addClass('app-team-member');
                
                $contactMenu = $contactSection->addContextMenu('popup');
                
                /**
                 * @todo Use Contact component controller
                 */
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($App->translate('View contact page'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->Contact()->display($contact->id)
                    )
                );
                
                /**
                 * @todo Use an access class
                 */
//                 if ($Access->updateContact($contact)) {
//                     $contactMenu->addItem(
//                         $W->Link(
//                             $W->Icon($Crm->translate('Edit contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
//                             $Crm->Controller()->Contact()->edit($contact->id)
//                         )
//                     );
//                 }
//                 if ($Access->emailContact($contact) && ($email = $contact->getMainEmail())) {
//                     $contactMenu->addItem(
//                         $W->Link(
//                             $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
//                             $Crm->Controller()->Email()->edit($contact->getMainEmail(), null, null, $contact->getRef() . ',' . $deal->getRef())
//                             )
//                         );
//                 }
                
                $contactsLayout->addItem($contactSection);
                    
            }
            $dealFrame->addItem($contactsLayout);
            
            $lead = $deal->lead();
            if (isset($lead)) {
                $orgs[] = $lead;
            }
            
            return $frame;
        }
    }
    
    function organizations($orgs)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $Ui->includeOrganization();
        
        $orgs += $this->task->getLinkedOrganizations();
        if (0 === count($orgs)) {
            return null;
        }
        
        $objectsFrame = $W->Frame();
        
        $objectsFrame->addItem(
            $W->Title($App->translate('Organizations linked to this task'), 4)
            ->setSizePolicy(\Widget_SizePolicy::MAXIMUM)
        );
        
        foreach ($orgs as $org) {            
            $card = $Ui->OrganizationCardFrame($org);
            
            $organizationFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('app-card');
            $objectsFrame->addItem($organizationFrame);
            
            $organizationFrame->addItem($card);
            
            $orgContacts = $org->selectContacts();
            
            $contactsLayout = $W->Section(
                $App->translate('Contacts in this organization'),
                $W->VBoxLayout()->setVerticalSpacing(2, 'px'),
                4
            )->setFoldable(true, true);
                
            foreach ($orgContacts as $orgContact) {
                
                $contact = $orgContact->getContact();
                
                $contactCard = $Ui->ContactCardFrame(
                    $contact
                );
                
                $contactSection = $W->Section(
                    $contact->getFullName(),
                    $contactCard,
                    6
                )->setFoldable(true, true);
                
                $contactSection->setSubHeaderText($contact->getMainPosition());
                $contactSection->addClass('app-team-member');
                
                $contactMenu = $contactSection->addContextMenu('popup');
                
                /**
                 * @todo Use Contact component controller
                 */
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($App->translate('View contact page'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->Contact()->display($contact->id)
                    )
                );
                
                /**
                 * @todo Use an access class
                 */
//                 if ($Access->updateContact($contact)) {
//                     $contactMenu->addItem(
//                         $W->Link(
//                             $W->Icon($Crm->translate('Edit contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
//                             $Crm->Controller()->Contact()->edit($contact->id)
//                         )
//                     );
//                 }
//                 if ($Access->emailContact($contact) && ($email = $contact->getMainEmail())) {
//                     $contactMenu->addItem(
//                         $W->Link(
//                             $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
//                             $Crm->Controller()->Email()->edit($contact->getMainEmail(), null, null, $contact->getRef() . ',' . $org->getRef())
//                         )
//                     );
//                 }
                
                $contactsLayout->addItem($contactSection);
                    
            }
            $organizationFrame->addItem($contactsLayout);
        }
        
        return $objectsFrame;
    }
}