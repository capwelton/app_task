<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;

use app_Link;


/**
 * A Task is a task to perform
 *
 * @property string         $summary
 * @property string         $description
 * @property string         $dueDate
 * @property float          $work               The planned work time to complete the task.
 * @property float          $actualWork         The actual work time for this task.
 * @property float          $remainingWork      The planned remaining work time to complete the task.
 * @property int            $completion         Percentage completion
 * @property string         $scheduledStart     The scheduled start date of the task
 * @property string         $scheduledFinish    The scheduled finish date of the task
 * @property boolean        $scheduledAllDay
 * @property int            $responsible        User id
 * @property string         $startedOn
 * @property string         $cancelledOn
 * @property string         $completedOn
 * @property int            $completedBy
 *
 * @property string             $startedAction
 * @property string             $completedAction
 * @property string             $canceledAction
 *
 * @property int                $sortkey
 *
 * @property Task           $parent             The parent task
 * @property TaskType       $type
 */
class Task extends \app_TraceableRecord
{
    public function getController()
    {
        $App = $this->App();
        return $App->Controller()->Task();
    }
    
    public function getRecordTitle()
    {
        return $this->summary;
    }
    
    /**
     * Test if a user can work on the task
     * @return bool
     */
    public function canUserWork($user = null)
    {
        if (!bab_isUserLogged()) {
            return false;
        }
        
        if (!isset($user)) {
            $user = bab_getUserId();
        }
        
        return ($this->responsible == $user);
    }
    
    
    
    public function isDuplicable()
    {
        return true;
    }
    
    
    /**
     * Get a widget with fullpath name, can be used in a link
     * @return \Widget_Displayable_Interface
     */
    public function getNameWidget()
    {
        $W = bab_Widgets();
        $name = $W->Items();
        $separator = '';
        
        foreach($this->getPathName() as $summary) {
            $shortened = bab_abbr($summary, BAB_ABBR_FULL_WORDS, 100);
            
            $label = $W->Label($separator.$shortened);
            if ($shortened !== $summary) {
                $label->setTitle($summary);
            }
            
            $name->addItem($label);
            $separator = ' > ';
        }
        
        return $name;
    }
    
    
    /**
     * @return array
     */
    public function getPathName()
    {
        $path = array();
        
        
        $ancestors = $this->getAncestors();
        foreach($ancestors as $task) {
            array_push($path, $task->summary);
        }
        
        array_push($path, $this->summary);
        
        return $path;
    }
    
    
    /**
     *
     * @return array
     */
    public function getAncestors()
    {
        $p = $this->parent();
        
        if (!$p) {
            return array();
        }
        
        $list = $p->getAncestors();
        $list[] = $p;
        
        return $list;
    }
    
    
    /**
     * Check if all work is done one the task
     * @return bool
     */
    public function isCompleted()
    {
        $work = (int) round(100 * $this->work);
        $remainingWork = (int) round(100 * $this->remainingWork);
        
        $simpleCompletedTodo = (100 === (int) $this->completion);
        $completedWorkload = 0 === $remainingWork && $work > 0;
        
        return ($simpleCompletedTodo || $completedWorkload);
    }
    
    
    /**
     * Check if some work has been done on the task
     * @return bool
     */
    public function hasStarted()
    {
        $work = (int) round(100 * $this->work);
        $actualWork = (int) round(100 * $this->actualWork);
        $remainingWork = (int) round(100 * $this->remainingWork);
        
        if ($work || $actualWork) {
            return ($remainingWork < $work);
        }
        
        return (0 !== (int) $this->completion);
    }
    
    /**
     * Checks if the task is not completed and the scheduledFinish or dueDate is in the past.
     * @return boolean
     */
    public function isLate()
    {
        if ($this->isCompleted()) {
            return false;
        }
        if ($this->scheduledFinish !== '0000-00-00 00:00:00') {
            $now = \BAB_DateTime::now();
            $scheduledFinish = \BAB_DateTime::fromIsoDateTime($this->scheduledFinish);
            return \BAB_DateTime::compare($now, $scheduledFinish) > 0;
        }
        if ($this->dueDate !== '0000-00-00') {
            $now = \BAB_DateTime::now();
            $dueDate = \BAB_DateTime::fromIsoDateTime($this->dueDate . ' 00:00:00');
            return \BAB_DateTime::compare($now, $dueDate) > 0;
        }
        return false;
    }
    
    
    /**
     *
     * @param int	$completion [0-100]
     * @return Task
     */
    public function setCompletion($completion)
    {
        $this->completion = $completion;
        
        $this->save();
        return $this;
    }
    
    
    
    /**
     * Get computed completion percentage or completion if the task is a simple TODO
     * @return int
     */
    public function getCompletion()
    {
        if (!$this->isPlanned()) {
            // No work duration
            return (int) $this->completion;
        }
        
        if (round($this->remainingWork * 100) == 0)  {
            return 100;
        }
        
        $total = $this->actualWork + $this->remainingWork;
        
        if (round($total * 100) <= 0) {
            return 0;
        }
        
        return (int) round(($this->actualWork * 100) / $total);
    }
    
    
    
    /**
     * Get completion average on sub-Tasks or completion if there are no sub-tasks
     * @return int
     */
    public function getTreeCompletion()
    {
        return $this->getTreeAvg('getCompletion');
    }
    
    
    /**
     * Test if there is planned quantity
     * @return bool
     */
    public function isPlanned()
    {
        return (0 !== (int) round(100 * $this->getWork()));
    }
    
    
    /**
     * The planned work time to complete the task.
     * @return float
     */
    public function getWork()
    {
        return (float) $this->work;
    }
    
    
    /**
     * Get work sum on sub-Tasks or work if there are no sub-tasks
     * @return int
     */
    public function getTreeWork()
    {
        return $this->getTreeSum('getWork');
    }
    
    
    /**
     * The actual work time for this task.
     * @return float
     */
    public function getActualWork()
    {
        return (float) $this->actualWork;
    }
    
    /**
     * Get actual work sum on sub-Tasks or actual work if there are no sub-tasks
     * @return int
     */
    public function getTreeActualWork()
    {
        return $this->getTreeSum('getActualWork');
    }
    
    
    /**
     * The planned remaining work time to complete the task.
     * @return float
     */
    public function getRemainingWork()
    {
        return (float) $this->remainingWork;
    }
    
    /**
     * Get remaining work sum on sub-Tasks or remaining work if there are no sub-tasks
     * @return int
     */
    public function getTreeRemainingWork()
    {
        return $this->getTreeSum('getRemainingWork');
    }
    
    
    /**
     * Returns the numbers of days left before the due date of the task.
     *
     * @param \BAB_DateTime	$date		The date or null for now.
     * @return int
     */
    public function getRemainingDays($date = null)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        if (!isset($date)) {
            $date = \BAB_DateTime::now();
        }
        $nbDays = \BAB_DateTime::dateDiffIso($this->dueDate, $date->getIsoDate());
        return $nbDays;
    }
    
    
    /**
     *
     */
    public function linkTo(\app_Record $source, $linkType = 'requiresTask')
    {
        if(!$this->isLinkedTo($source, $linkType)){
            parent::linkTo($source, $linkType);
        }
    }
    
    
    /**
     * @return \app_Record[]
     */
    public function getLinkedContacts($linkType = 'requiresTask')
    {
        $App = $this->App();
        
        /**
         * @todo Make use of Contact component
         */
        $App->includeContactSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->ContactClassName(), $linkType);
        
        $contacts = array();
        foreach ($links as $link) {
            $contacts[] = $link->sourceId;
        }
        
        return $contacts;
    }
    
    /**
     * @return \app_Record[]
     */
    public function getLinkedOrganizations($linkType = 'requiresTask')
    {
        $App = $this->App();
        
        /**
         * @todo Make use of Organization component
         */
        $App->includeOrganizationSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->OrganizationClassName(), $linkType);
        
        $orgs = array();
        foreach ($links as $link) {
            if ($link->sourceId) {
                $orgs[] = $link->sourceId;
            }
        }
        
        return $orgs;
    }
    
    /**
     * @return \app_Record[]
     */
    public function getLinkedDeals($linkType = 'requiresTask')
    {
        $App = $this->App();
        
        /**
         * @todo Make use of Deal component
         */
        $App->includeDealSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->DealClassName(), $linkType);
        
        $deals = array();
        foreach ($links as $link) {
            $deals[] = $link->sourceId;
        }
        
        return $deals;
    }
    
    
    /**
     * @return \app_Record[]
     */
    public function getLinkedTeams($linkType = 'requiresTask')
    {
        $App = $this->App();
        
        /**
         * @todo Make use of Team component
         */
        $App->includeTeamSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->TeamClassName(), $linkType);
        
        $teams = array();
        foreach ($links as $link) {
            $teams[]   = $link->sourceId;
        }
        
        return $teams;
    }
    
    
    
    /**
     * @return \app_Record[]
     */
    public function getLinkedArticles($linkType = 'requiresTask')
    {
        $App = $this->App();
        
        /**
         * @todo Make use of Article component
         */
        $App->includeArticleSet();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $App->ArticleClassName(), $linkType);
        
        $orderItems = array();
        foreach ($links as $link) {
            $orderItems[]   = $link->sourceId;
        }
        
        return $orderItems;
    }
    
    
    
    
    /**
     * Get linked records
     * @return \app_Record[]
     */
    public function getLinkedObjects($linkType = 'requiresTask')
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, null, $linkType);
        
        $records = array();
        foreach ($links as $link) {
            /*@var $link \app_Link */
            $records[] = $link->getSource();
        }
        
        return $records;
    }
    
    
    /**
     * get linked records of the same set
     * @param \app_RecordSet $recordSet
     * @return \app_Record[]
     */
    public function getLinkedRecords(\app_RecordSet $recordSet, $linkType = 'requiresTask')
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        
        $links = $linkSet->selectForTarget($this, $recordSet->getRecordClassName(), $linkType);
        
        $records = array();
        foreach ($links as $link) {
            $records[] = $link->sourceId;
        }
        
        return $records;
    }
    
    
    public function saveUnique()
    {
        $taskSet = $this->getParentSet();
        
        $conditions = array();
        $conditions[] = $taskSet->startedAction->is($this->startedAction);
        $conditions[] = $taskSet->responsible->is($this->responsible);
        $conditions[] = $taskSet->isNotCompleted();
        
        $task = $taskSet->get($taskSet->all($conditions));
        
        if (!isset($task)) {
            return $this->save();
        }
        
        return false;
    }
    
    
    
    /**
     * Select associated tasks.
     *
     * @return \ORM_Iterator
     */
    public function selectChildTasks()
    {
        $taskSet = $this->getParentSet();
        $res = $taskSet->select($taskSet->parent->is($this->id));
        $res->orderAsc($taskSet->sortkey);
        return $res;
    }
    
    
    
    /**
     * This method should be overwridden in your App
     * @return bool
     */
    public function canDeleteChildTasks()
    {
        return false;
    }
    
    
    /**
     * Get a sum from a method o all sub-Tasks
     * @param string $method
     * @return Number
     */
    protected function getTreeSum($method)
    {
        $res = $this->selectChildTasks();
        if (0 === $res->count()) {
            return $this->$method();
        }
        
        $sum = 0;
        foreach($res as $subTask) {
            $sum += $subTask->getTreeSum($method);
        }
        
        return $sum;
    }
    
    
    /**
     * Get a average from a method o all sub-Tasks
     * @param string $method
     * @return Number
     */
    protected function getTreeAvg($method)
    {
        $res = $this->selectChildTasks();
        if (0 === $res->count()) {
            return $this->$method();
        }
        
        $sum = 0;
        foreach($res as $subTask) {
            $sum += $subTask->getTreeSum($method);
        }
        
        return $sum/$res->count();
    }
    
    
    /**
     * Creates a vevent icalendar string based on the task.
     *
     * @return string
     */
    public function getVevent()
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';
        
        $caldav = \bab_functionality::get('CalendarBackend/Caldav');
        $caldav->includeCalendarPeriod();
        
        $scheduled = false;
        $due = false;
        if ($this->scheduledStart !== '0000-00-00 00:00:00') {
            $scheduled = true;
            $start = \BAB_DateTime::fromIsoDateTime($this->scheduledStart);
            $end = \BAB_DateTime::fromIsoDateTime($this->scheduledFinish);
        } elseif ($this->dueDate !== '0000-00-00') {
            $due = true;
            $start = \BAB_DateTime::fromIsoDateTime($this->dueDate . ' 00:00:00');
            $end = \BAB_DateTime::fromIsoDateTime($this->dueDate . '  00:00:00');
            $end->add(1, BAB_DATETIME_DAY);
        }
        
        $lastModified = \BAB_DateTime::fromIsoDateTime($this->modifiedOn);
        
        $calendarPeriod = new \bab_CalendarPeriod();
        
        if ($scheduled) {
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('DTSTART', $start->getICal(true));
            $calendarPeriod->setProperty('DTEND', $end->getICal(true));
        } elseif ($due) {
            $calendarPeriod->setProperty('DTSTART;VALUE=DATE', date('Ymd', bab_mktime($this->dueDate . ' 00:00:00')));
        }
        $calendarPeriod->setProperty('DTSTAMP', $lastModified->getICal(true));
        //$calendarPeriod->setProperty('ORGANIZER;CN='.bab_getUserName(1), 'mailto:' . bab_getUserEmail(1));
        $calendarPeriod->addAttendeeByUserId($this->responsible, 'REQ-PARTICIPANT', 'NEEDS-ACTION', 'FALSE');
        
        $calendarPeriod->setProperty('DESCRIPTION', $this->description);
        
        $calendarPeriod->setProperty('LAST-MODIFIED', $lastModified->getICal(true));
        
        $calendarPeriod->setProperty('SEQUENCE', '0');
        $calendarPeriod->setProperty('STATUS', 'CONFIRMED');
        $calendarPeriod->setProperty('SUMMARY', $this->summary);
        $calendarPeriod->setProperty('LOCATION', '');
        
        $calendarPeriod->setProperty('UID', $this->uuid);
        
        $vEvent = \caldav_CalendarPeriod::toIcal($calendarPeriod);
        
        return $vEvent;
    }
    
    
    /**
     * Sends an invitation in an email notification to the task responsible.
     *
     */
    public function sendNotification()
    {
        $App = $this->App();
        
        $contactSet = $App->ContactSet();
        
        $Spooler = @\bab_functionality::get('Mailspooler');
        
        $caldav = \bab_functionality::get('CalendarBackend/Caldav');
        
        $caldav->includeCalendarPeriod();
        
        $mail = bab_mail();
        
        if (!$mail) {
            return;
        }
        
        $emailTitle = $this->summary;
        
        $emailMessage = '';
        $deals = $this->getLinkedDeals(null);
        foreach ($deals as $deal) {
            $emailMessage .= $App->translate('Deal') . ' : ' . $deal->getFullNumber() . "\n" . $deal->name . "\n\n";
        }
        
        $emailMessage .= $this->description;
        
        
        $mail->mailSubject($emailTitle);
        
        $htmlMessage= $mail->mailTemplate(bab_toHtml($emailMessage, BAB_HTML_BR));
        $mail->mailBody($htmlMessage);
        $mail->mailAltBody($emailMessage);
        
        $mail->clearAllRecipients();
        
        $vCalendar = 'BEGIN:VCALENDAR' . "\r\n"
            . 'METHOD:REQUEST' . "\r\n"
            . 'VERSION:2.0' . "\r\n"
            . $caldav->getProdId() . "\r\n"
            . $caldav->getTimeZone()
            . $this->getVevent() . "\r\n"
            . 'END:VCALENDAR';
                            
        $mail->mail->Ical = $vCalendar;
        
        $recipients = array();
        $taskAttendees = $this->getAttendees();
        foreach ($taskAttendees as $taskAttendee) {
            $attendeeRecord = $App->getRecordByRef($taskAttendee->attendee);
            if (! ($attendeeRecord instanceof \Isi4You_Contact)) {
                continue;
            }
            $recipients[] = $attendeeRecord->getMainEmail();
        }
        
        if (count($recipients) == 0) {
            return;
        }
        
        foreach ($recipients as $recipient) {
            $mail->mailTo($recipient);
        }
        
        /* @var $Spooler Func_Mailspooler */
        if ($Spooler) {
            $Spooler->save($mail);
        }
        $mail->send();
    }
    
    
    /**
     * 
     * @return unknown
     */
    public function getAttendees()
    {
        $App = $this->App();
        $taskAttendeeComponent = $App->getComponentByName('TaskAttendee');
        $taskAttendeeSet = $taskAttendeeComponent->recordSet();
        
        return $taskAttendeeSet->select($taskAttendeeSet->task->is($this->id));
    }
    
    public function getBlueprint()
    {
        $type = $this->type();
        if(!empty($type->id)){
            return $type->getBlueprint();
        }
        
        $typeSet = $this->App()->TaskTypeSet();
        $dummy = $typeSet->newRecord();
        $dummy->id = 0;
        
        return $dummy->getBlueprint();
    }
    
    /**
     * @return \app_Link[]
     */
    public function getLinks()
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();
        
        return $linkSet->select(
            $linkSet->targetClass->is(get_class($this))
            ->_AND_($linkSet->targetId->is($this->id))
            ->_AND_($linkSet->type->is('requiresTask'))
        );
    }
}
