<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;


/**
 * A TaskType is a type that can be associated to any task.
 *
 * @property string		$name
 */
class TaskType extends \app_TraceableRecord
{
    public function getBlueprint()
    {
        return $this->getRef();
    }
    
    public function setDefaultBlueprint()
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();
        $customSection = $customSectionSet->get($customSectionSet->object->is('Task')->_AND_($customSectionSet->view->is($this->getBlueprint())));
        if(!isset($customSection)){
            $customSection = $customSectionSet->newRecord();
            $customSection->object = 'Task';
            $customSection->view = $this->getBlueprint();
        }
        
        $customSection->name = $App->translate('Details');
        $customSection->fields = 'description';
        $customSection->sizePolicy = 'col-md-12';
        $customSection->classname = 'box';
        $customSection->fieldsLayout = 'verticalLabel';
        $customSection->foldable = false;
        $customSection->folded = false;
        $customSection->editable = true;
        $customSection->rank =1;
        $customSection->save();
        
        return $this;
    }
}
