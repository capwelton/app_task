<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;


/**
 * @property string             $type
 * @property string             $attendee
 *
 * @property Task        $task
 * @method Task          task()
 *
 * @method AlarmSet getParentSet()
 *
 * @method \Func_App    App()
 */
class TaskAttendee extends \app_TraceableRecord
{
    const TYPE_MEMBER_MANDATORY = 'membermandatory';
    const TYPE_MEMBER_OPTIONAL  = 'memberoptional';
    const TYPE_ORGANISER        = 'organiser';

    public static function getTypes()
    {
        $App = app_App();
        return array(
            self::TYPE_MEMBER_MANDATORY => $App->translate('Mandatory member'),
            self::TYPE_MEMBER_OPTIONAL  => $App->translate('Optional member'),
            self::TYPE_ORGANISER        => $App->translate('Organiser'),
        );
    }
    
    public function task()
    {
        $App = $this->App();
        $set = $App->TaskSet();
        $set->setDefaultCriteria($set->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        
        return $set->get($this->task);
    }
    
    public function attendee()
    {
        $App = $this->App();
        return $App->getRecordByRef($this->attendee);
    }
    
    public function getTypeIcon()
    {
        switch ($this->type){
            case self::TYPE_MEMBER_MANDATORY:
                return 'icon apps-users';
                break;
            case self::TYPE_ORGANISER:
                return 'icon actions-user-properties';
                break;
            case self::TYPE_MEMBER_OPTIONAL:
            default:
                return 'icon apps-users-o';
                break;
        }
    }
    
    public function getTypeTitle()
    {
        $types = self::getTypes();
        
        return isset($types[$this->type]) ? $types[$this->type] : $this->App()->translate('Unknown');
    }
}
