<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;

/**
 * A TaskType is a type associated to tasks.
 *
 * @property ORM_StringField	$name
 */
class TaskTypeSet extends \app_TraceableRecordSet
{

    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'TaskType');
        
        $this->setDescription('Task type');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('name')->setDescription('Name'),
            ORM_BoolField('isDefault')->setOutputOptions($App->translate('No'), $App->translate('Yes'))
        );
    }
    
    
    /**
     * {@inheritDoc}
     * @see \app_Record::isReadable()
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }
    
    public function isUpdatable()
    {
        return $this->all();
    }
    
    public function isDeletable()
    {
        return $this->all();
    }
    
    public function getDefaultBlueprint()
    {
        $component = $this->App()->getComponentByName('TaskType');
        return $component->getRecordClassName().':0';
    }
    
    /**
     * @see \app_Component::onUpdate()
     */
    public function onUpdate()
    {
        $App = $this->App();
        $customSectionSet = $App->CustomSectionSet();
        
        $defaultViewName = $this->getDefaultBlueprint();
        
        $defaultSection = $customSectionSet->get(
            $customSectionSet->object->is('Task')
            ->_AND_($customSectionSet->view->is($defaultViewName))
        );
        
        if(!$defaultSection){
            $defaultSection = $customSectionSet->newRecord();
            $defaultSection->object = 'Task';
            $defaultSection->view = $defaultViewName;
            $defaultSection->fields = 'description';
            $defaultSection->sizePolicy = 'col-md-12';
            $defaultSection->classname = 'box';
            $defaultSection->fieldsLayout = 'verticalLabel';
            $defaultSection->save();
        }
    }
}