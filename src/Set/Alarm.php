<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;


/**
 * @property string             $trigger
 * @property string             $type
 * @property string             $attendees
 * @property bool               $sent
 * @property datetime           $sentOn
 *
 * @property Task        $task
 * @method Task          task()
 *
 * @method AlarmSet getParentSet()
 *
 * @method \Func_App    App()
 */
class Alarm extends \app_TraceableRecord
{

    const TYPE_EMAIL = 'email';
    const TYPE_DISPLAY = 'display';

    public static function getTypes()
    {
        $App = app_App();
        return array(
            self::TYPE_EMAIL    => $App->translate('Email'),
            self::TYPE_DISPLAY  => $App->translate('Display'),
        );
    }




    /**
     * Return the date/time of alarm base on task dueDate and alarm trigger.
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        $App = $this->App();
        $App->includeTaskSet();

        $task = $this->task();
        if (!$task->dueDate) {
            return null;
        }
        $date = $task->dueDate;


        $date = \DateTime::createFromFormat('Y-m-d', $date);
        if(!empty($this->trigger)){
            $date->sub(new \DateInterval($this->trigger));
        }

        return $date;
    }
    
    public function task()
    {
        $App = $this->App();
        $set = $App->TaskSet();
        $set->setDefaultCriteria($set->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        
        return $set->get($this->task);
    }
    
    public function shouldBeSent()
    {
        if($this->sent){
            return false;
        }
        
        $today = new \DateTime('TODAY'); //Using TODAY instead of NOW to set the time at 00:00:00
        $mustBeSendOn = $this->getDateTime();
        
        return $mustBeSendOn <= $today;
    }
}
