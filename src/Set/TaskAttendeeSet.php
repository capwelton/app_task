<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;


/**
 * @property ORM_StringField    $trigger
 * @property ORM_EnumField      $type
 * @property ORM_StringField    $attendees
 *
 * @property Task     $task
 * @method Task       task()
 *
 * @method Alarm     get(mixed $criteria)
 * @method Alarm     request(mixed $criteria)
 * @method Alarm[]   select(\ORM_Criteria $criteria = null)
 * @method Alarm     newRecord()
 *
 * @method Func_App    App()
 */
class TaskAttendeeSet extends \app_TraceableRecordSet
{
    /**
     *
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'TaskAttendee');
        
        $this->setDescription('TaskAttendee');
        
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_EnumField('type', TaskAttendee::getTypes())->setDescription('Type'),
            ORM_StringField('attendee')->setDescription('Attendee')
        );

        $taskComponent = $App->getComponentByName('Task');
        if(!$taskComponent){
            throw new \app_Exception('The component Task must be added to the App for the Alarm component to work');
        }
        
        $this->hasOne('task', $taskComponent->getSetClassName());
    }


    public function isCreatable()
    {
        return true;
    }

    public function isReadable()
    {
        return $this->all();
    }

    public function isDeletable()
    {
        return $this->all();
    }

    public function isUpdatable()
    {
        return $this->all();
    }
}