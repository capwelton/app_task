<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */
namespace Capwelton\App\Task\Set;

/**
 * @property ORM_StringField    $summary
 * @property ORM_TextField      $description
 * @property ORM_DateField      $dueDate
 * @property ORM_DecimalField   $work
 * @property ORM_DecimalField   $actualWork
 * @property ORM_DecimalField   $remainingWork
 * @property ORM_IntField       $completion
 * @property ORM_DateTimeField  $scheduledStart
 * @property ORM_DateTimeField  $scheduledFinish
 * @property ORM_BoolField      $scheduledAllDay
 * @property ORM_DateTimeField  $startedOn
 * @property ORM_DateTimeField  $completedOn
 * @property ORM_DateTimeField  $canceledOn
 * @property ORM_StringField    $startedAction
 * @property ORM_StringField    $completedAction
 * @property ORM_StringField    $canceledAction
 * @property ORM_UserField      $responsibleUser
 * @property ORM_UserField      $completedBy
 * @property ORM_IntField       $sortkey
 * @property Task               $parent
 * @property TaskType           $type
 * @property ORM_BoolField      $send     
 * @property ORM_BoolField      $milestone
 * @property ORM_BoolField      $cir
 * @property ORM_BoolField      $isBillable         A facturer
 */
class TaskSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Task');
        
        $this->setDescription('Task');
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('summary')->setDescription($App->translatable('Summary')),
            ORM_TextField('description')->setDescription($App->translatable('Description')),
            ORM_DateField('dueDate')->setDescription($App->translatable('Due date')),
            ORM_DecimalField('work')->setDescription($App->translatable('Planned work time in hours')),
            ORM_DecimalField('actualWork')->setDescription('Actual work time in hours'),
            ORM_DecimalField('remainingWork')->setDescription('Remaining work time in hours'),
            ORM_IntField('completion')->setDescription('Completion percentage'),
            ORM_DateTimeField('scheduledStart')->setDescription('The scheduled start date of the task'),
            ORM_DateTimeField('scheduledFinish')->setDescription('The scheduled finish date of the task'),
            ORM_BoolField('scheduledAllDay')->setDescription('scheduled all day, ignore hours in scheduled period'),
            ORM_DateTimeField('startedOn')->setDescription('Start date and time'),
            ORM_DateTimeField('completedOn')->setDescription('Completion date and time'),
            ORM_DateTimeField('canceledOn')->setDescription('Cancelation date and time'),
            ORM_StringField('startedAction')->setDescription('url of action executed when the task is started'),
            ORM_StringField('completedAction')->setDescription('url of action executed when the task is completed'),
            ORM_StringField('canceledAction')->setDescription('url of action executed when the task is canceled'),
            ORM_UserField('responsibleUser')->setDescription('Responsible user'),
            ORM_UserField('completedBy')->setDescription('The user who completed the task'),
            ORM_IntField('sortkey')->setDescription('Position of task in the list of sibblings tasks under the summary task designated by the parent field'),
            ORM_StringField('attendees')->setDescription($App->translatable('Attendees')),
            ORM_BoolField('private')
                ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                ->setDescription($App->translatable('Private')),
            ORM_BoolField('send')
                ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                ->setDescription($App->translatable('Add to calendar')),
            ORM_BoolField('milestone')
                ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                ->setDescription($App->translatable('Milestone')),
            ORM_BoolField('cir')
                ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                ->setDescription($App->translatable('CIR')),
            ORM_BoolField('isBillable')
                ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                ->setDescription($App->translatable('Is billable'))
        );
        
        $taskTypeComponent = $App->getComponentByName('TaskType');
        if(!$taskTypeComponent){
            throw new \app_Exception('The component TaskType must be added to the App for the Task component to work');
        }
        $alarmComponent = $App->getComponentByName('Alarm');
        if(!$alarmComponent){
            throw new \app_Exception('The component Alarm must be added to the App for the Task component to work');
        }
        $taskAttendeeComponent = $App->getComponentByName('TaskAttendee');
        if(!$taskAttendeeComponent){
            throw new \app_Exception('The component TaskAttendee must be added to the App for the Task component to work');
        }

        $this->hasOne('responsible', $App->ContactSetClassName())->setDescription('The collaborator responsible for this task');
        $this->hasOne('parent', $this->getTableName().'Set');
        $this->hasOne('type', $taskTypeComponent->getSetClassName())->setDescription('Type');
        
        $this->hasMany('alarms', $alarmComponent->getSetClassName(), 'task')->setOnDeleteMethod(\ORM_ManyRelation::ON_DELETE_CASCADE);
        $this->hasMany('attendees', $taskAttendeeComponent->getSetClassName(), 'task')->setOnDeleteMethod(\ORM_ManyRelation::ON_DELETE_CASCADE);
    }
    
    public function getRequiredComponents()
    {
        return array(
            'TaskType',
            'Alarm',
            'TaskAttendee'
        );
    }
    
    public function getOptionalComponents()
    {
        return array(
            'Note'
        );
    }
    
    
    /**
     * @return \ORM_Criterion
     */
    public function isNotCompleted()
    {
        return $this->completion->lessThan(100);
    }
    
    /**
     * @return \ORM_Criterion
     */
    public function isCompleted()
    {
        return $this->completion->greaterThanOrEqual(100);
    }
    
    
    /**
     * Returns an iterator of tasks linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @return \ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = 'requiresTask')
    {
        return parent::selectLinkedTo($source, $linkType);
    }
    
    
    /**
     * Checks if the task is not completed and the scheduledFinish or dueDate is in the past.
     * @return \ORM_Criteria
     */
    public function isLate()
    {
        return $this->all(
            $this->isNotCompleted(),
            $this->any(
                $this->scheduledFinish->isNot('0000-00-00 00:00:00')->_AND_($this->scheduledFinish->lessThan(date('Y-m-d H:i:s'))),
                $this->dueDate->isNot('0000-00-00')->_AND_($this->dueDate->lessThan(date('Y-m-d')))
            )
        );
    }
    
    /**
     * 
     * @param \ORM_Record $contact
     * @param string $type
     * @return \ORM_InCriterion
     */
    public function hasAttendee($contact, $type = null)
    {
        $App = $this->App();
        $taskAttendeeComponent = $App->getComponentByName('TaskAttendee');
        $taskAttendeeSet = $taskAttendeeComponent->recordSet();

        $conditions = array(
            $taskAttendeeSet->deleted->isNot(true),
            $taskAttendeeSet->attendee->is($contact->getRef())
        );
        if (isset($type)) {
            $conditions[] = $taskAttendeeSet->type->is($type);
        }
        return $this->id->in($taskAttendeeSet->all($conditions), 'task');
    }
    
    /**
     * {@inheritDoc}
     * @see \app_TraceableRecordSet::isReadable()
     */
    public function isReadable()
    {
        return $this->any(
            $this->private->is(false),
            $this->private->is(true)->_AND_($this->createdBy->is(bab_getUserId()))
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }
    
    public function isUpdatable()
    {
        return $this->all();
    }
    
    public function isDeletable()
    {
        return $this->all();
    }
}