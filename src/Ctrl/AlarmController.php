<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ctrl;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Ui\TaskUi;
use Capwelton\App\Task\Ui\UserTasks;
use Capwelton\App\Task\Set\Alarm;
use Capwelton\App\Task\Set\TaskAttendee;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on alarms.
 *
 * @method Func_App App()
 */
class AlarmController extends \app_ComponentCtrlRecord
{
    /**
     *
     * @param int $forTask       The task id
     */
    public function listFor($for, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $taskSet = $App->TaskSet();
        $taskSet->setDefaultCriteria($taskSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
//         $taskSet->setAllowedStatuses(null);
        $task = $taskSet->request($for);
        


        $alarmSet = $App->AlarmSet();
//         $alarmSet->setAllowedStatuses(array(crm_TraceableRecord::DELETED_STATUS_EXISTING, crm_TraceableRecord::DELETED_STATUS_DRAFT));
        $alarms = $alarmSet->select($alarmSet->task->is($task->id));

        foreach ($alarms as $alarm) {

            $dateTime = $alarm->getDateTime();
            if ($dateTime) {
                $date = bab_shortDate($dateTime->getTimestamp(), true);
            } else {
                $date = '--';
            }

            $element = $App->getRecordByRef($alarm->attendees);
            $attendees = '-';
            $attendeesClass = '';
            if (isset($element)) {
                $attendees = $element->getRecordTitle();
                /**
                 * @todo Class depending on element type
                 */
//                 if ($element instanceof crmbtp_Team) {
//                     $attendeesClass = \Func_Icons::OBJECTS_GROUP;
//                 } elseif ($element instanceof crmbtp_Contact) {
//                     $attendeesClass = \Func_Icons::OBJECTS_CONTACT;
//                 }
            }

            if ($alarm->type === Alarm::TYPE_EMAIL) {
                $alarmClass = \Func_Icons::ACTIONS_MAIL_SEND;
            } else {
                $alarmClass = \Func_Icons::OBJECTS_WAITING_ITEM;
            }

            $line = $W->FlowItems(
//                 $W->Label($alarm->type)
//                     ->setSizePolicy('widget-10pc'),
                $W->Label($date . ' (' . $App->translate($alarm->trigger) . ')')
                    ->addClass('icon', $alarmClass)
                    ->setSizePolicy('widget-40pc'),
                $W->Label($attendees)
                    ->addClass('icon', $attendeesClass)
                    ->setSizePolicy('widget-40pc'),
                $W->FlowItems(
                    $W->Link(
                        '', $this->proxy()->edit($alarm->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    $W->Link(
                        '', $this->proxy()->confirmDelete($alarm->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                )->addClass('widget-actions', 'widget-align-right')
                ->setSizePolicy('widget-20pc')
            );
            $line->addClass('widget-actions-target');
//             $line->setSizePolicy('widget-list-element');

            $box->addItem($line);
        }
        $box->addClass(\Func_Icons::ICON_LEFT_16);

        $box->setReloadAction($this->proxy()->listFor($for, $limit, $box->getId()));
        $box->addClass('depends-'.$this->getRecordClassName());
        return $box;
    }


    /**
     * @return \app_Editor
     */
    protected function recordEditor(Alarm $alarm, $itemId = null)
    {
        $App = $this->App();
        /* @var $Ui TaskUi */
        $Ui = $App->Task()->Ui();
        $W = bab_Widgets();
        
        $recordSet = $this->getEditRecordSet();
        
        $editor = new \app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem($W->Hidden()->setName('id'));
        $editor->addItem($W->Hidden()->setName('task'));
        
        
        $triggerSelect = $W->Select();
        $triggerSelect->addOption('', '');
        $triggerSelect->addOption('P1D', $App->translate('1 day before'));
        $triggerSelect->addOption('P2D', $App->translate('2 days before'));
        $triggerSelect->addOption('P3D', $App->translate('3 days before'));
        $triggerSelect->addOption('P4D', $App->translate('4 days before'));
        $triggerSelect->addOption('P5D', $App->translate('5 days before'));
        $triggerSelect->addOption('P1W', $App->translate('1 week before'));
        
        $triggerDuration = $W->NumberEdit()->setMin(0);
        $triggerUnit = $W->Select();
        $triggerUnit->addOption('i', $App->translate('Minutes'));
        $triggerUnit->addOption('h', $App->translate('Hours'));
        $triggerUnit->addOption('d', $App->translate('Days'));
        $triggerUnit->addOption('w', $App->translate('Weeks'));
        $triggerUnit->addOption('m', $App->translate('Months'));

        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Type'),
                \app_OrmWidget($recordSet->type),
                'type'
            )
        );
        $editor->addItem(
            $W->VBoxItems(
                $W->Label($App->translate('When'))->addClass('widget-description'),
                $W->HBoxItems(
                    $W->LabelledWidget(
                        '',
                        $triggerDuration,
                        'triggerDuration'
                    ),
                    $W->LabelledWidget(
                        '',
                        $triggerUnit,
                        'triggerUnit'
                    )
                ),
                $W->Label($App->translate('Defines when a notification will be sent'))->addClass('widget-long-description')
            )
        );
        
        $select = $W->Select();
        $select->addOptions(
            array(
                'all' => $App->translate('All participants'),   
                'organizer' => $App->translate('Organizers'),   
                'mandatory' => $App->translate('Mandatory members'),   
                'optional' => $App->translate('Optional members'),   
                'other' => $App->translate('Other'),   
            )
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Notified persons'),
                $select,
                'attendeesType'
            )
        );
        
        $editor->addItem(
            $suggest = $W->LabelledWidget(
                $App->translate('Other'),
                $suggest = $Ui->SuggestParticipant($alarm->task)->setMinChars(0),
                'attendees'
            )
        );
        
        $attendee = $App->getRecordByRef($alarm->attendees);
        if($attendee){
            $suggest->setValue($attendee);
        }
        
        $select->setAssociatedDisplayable($suggest, array('other'));
        
        if(isset($itemId)){
            $editor->setId($itemId);
        }
        $editor->isAjax = bab_isAjaxRequest();
        $editor->setSaveAction($this->proxy()->save());

        return $editor;
    }


    public function add($forTask)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();

        $taskSet = $App->TaskSet();
        $taskSet->setDefaultCriteria($taskSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
//         $taskSet->setAllowedStatuses(null);
        $task = $taskSet->request($forTask);

        $record = $recordSet->newRecord();
//         $record = $recordSet->newDraft();
        $record->type = Alarm::TYPE_EMAIL;
        $record->task = $task->id;

        $page = $Ui->Page();
        $page->setTitle($App->translate('Add notification'));

        $editor = $this->recordEditor($record);

        $editor->setRecord($record);

        $page->addItem($editor);

        return $page;
    }


    /**
     *
     * @param int $id
     */
    public function edit($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $page = $Ui->Page();
        $page->setTitle($App->translate('Edit notification'));

        $editor = $this->recordEditor($record);
        $editor->setValue(array('data', 'id'), $record->id);
        $editor->setValue(array('data', 'task'), $record->task);
        $editor->setValue(array('data', 'type'), $record->type);
        $editor->setValue(array('data', 'attendeesType'), 'other');
        
        if(!empty($record->trigger)){
            if(substr($record->trigger, 0, 2) == 'PT'){
                $duration = substr($record->trigger, 2, strlen($record->trigger) - 3);
                $unitTmp = substr($record->trigger, -1);
                $unit = '';
                switch($unitTmp){
                    case 'M': //MINUTES
                        $unit = 'i';
                        break;
                    case 'H': //HOURS
                        $unit = 'h';
                        break;
                }
                if(!empty($unit)){
                    $editor->setValue(array('data', 'triggerDuration'), $duration);
                    $editor->setValue(array('data', 'triggerUnit'), $unit);
                }
            }
            elseif(substr($record->trigger, 0, 1) == 'P'){
                $duration = substr($record->trigger, 1, strlen($record->trigger) - 2);
                $unitTmp = substr($record->trigger, -1);
                $unit = '';
                switch($unitTmp){
                    case 'D': //DAYS
                        $unit = 'd';
                        break;
                    case 'W': //WEEKS
                        $unit = 'w';
                        break;
                    case 'M': //MONHS
                        $unit = 'm';
                        break;
                }
                if(!empty($unit)){
                    $editor->setValue(array('data', 'triggerDuration'), $duration);
                    $editor->setValue(array('data', 'triggerUnit'), $unit);
                }
            }
        }
        
        $page->addItem($editor);

        return $page;
    }


    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::save()
     */
    public function save($data = null)
    {
        $App = $this->App();
        $taskAttendeeSet = $App->TaskAttendeeSet();
        
        $trigger = '';
        
        if(isset($data['triggerUnit']) && !empty($data['triggerUnit']) && isset($data['triggerDuration']) && !empty($data['triggerDuration'])){
            switch($data['triggerUnit']){
                case 'i': //MINUTES
                    $trigger = 'PT'.$data['triggerDuration'].'M';
                    break;
                case 'h': //HOURS
                    $trigger = 'PT'.$data['triggerDuration'].'H';
                    break;
                case 'd': //DAYS
                    $trigger = 'P'.$data['triggerDuration'].'D';
                    break;
                case 'w': //WEEKS
                    $trigger = 'P'.$data['triggerDuration'].'W';
                    break;
                case 'm': //MONTHS
                    $trigger = 'P'.$data['triggerDuration'].'M';
                    break;
            }
        }
        
        $data['trigger'] = $trigger;
        
        switch($data['attendeesType']){
            case 'other':
                if (isset($data['attendees_ID'])) {
                    $data['attendees'] = $data['attendees_ID'];
                    unset($data['attendees_ID']);
                }
                
                return parent::save($data);
                break;
            default:
                $criteria = $taskAttendeeSet->task->is($data['task']);
                if($data['attendeesType'] == 'organizer'){
                    $criteria = $criteria->_AND_($taskAttendeeSet->type->is(TaskAttendee::TYPE_ORGANISER));
                }
                else if($data['attendeesType'] == 'mandatory'){
                    $criteria = $criteria->_AND_($taskAttendeeSet->type->is(TaskAttendee::TYPE_MEMBER_MANDATORY));
                }
                else if($data['attendeesType'] == 'optional'){
                    $criteria = $criteria->_AND_($taskAttendeeSet->type->is(TaskAttendee::TYPE_MEMBER_OPTIONAL));
                }
                
                $taskAttendees = $taskAttendeeSet->select($criteria);
                foreach ($taskAttendees as $taskAttendee){
                    $customData = $data;
                    unset($customData['attendees_ID']);
                    $customData['attendees'] = $taskAttendee->attendee;
                    parent::save($customData);
                }
                break;
        }
        
        return true;
    }
}
