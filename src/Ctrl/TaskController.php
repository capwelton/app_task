<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2020 by CAPWELTON ({@link http://www.capwelton.com})
 */

namespace Capwelton\App\Task\Ctrl;
use Capwelton\App\Task\Set\Task;
use Capwelton\App\Task\Ui\TaskSectionEditor;
use Capwelton\App\Task\Ui\TaskTypeTableView;
use Capwelton\App\Task\Ui\UserTasks;
use Capwelton\App\Task\Set\TaskSet;
use Capwelton\App\Task\Ui\TaskEditor;
use Capwelton\App\Task\Set\TaskAttendee;
use Capwelton\App\Task\Set\TaskTypeSet;

$App = app_App();
$App->includeRecordController();


/**
 * This controller manages actions that can be performed on tasks.
 *
 * @method TaskSet getRecordSet()
 */
class TaskController extends \app_ComponentCtrlRecord
{
    protected function getRecordSet($withDraft = false)
    {
        $recordSet = parent::getRecordSet();
        if($withDraft){
            $recordSet->setDefaultCriteria($recordSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        }
        return $recordSet;
    }
    
    private function createDraftRecord($for, $type = null)
    {
        $App = $this->App();
        $set = $this->getRecordSet();
        
        $draftRecord = $set->newRecord();
        $draftRecord->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
        
        if (!isset($type)) {
            if($taskTypeComp = $App->getComponentByName('TaskType')){
                /* @var $set TaskTypeSet */
                $set = $taskTypeComp->recordSet();
                $defaultType = $set->get($set->isDefault->is(true));
                if($defaultType){
                    $type = $defaultType->id;
                }
            }
        }
        
        if(isset($type)){
            $draftRecord->type = $type;
        }
        
        /**
         * @todo Use Contact component
         */
        $contactSet = $App->ContactSet();
        $currentContact = $contactSet->get($contactSet->user->is($App->getCurrentUser()));
        if ($currentContact) {
            $draftRecord->responsible = $currentContact->id;
        }
        
        $draftRecord->save();
        if ($currentContact) {
            $this->saveAttendee(array('id' => $draftRecord->id, 'newAttendee_ID' => $currentContact->getRef()));
//            $draftRecord->linkTo($currentContact, 'requiresTask');
        }
        
        $for = $App->getRecordByRef($for);
        if (isset($for)) {
            $draftRecord->linkTo($for, 'requiresTask');
        }
        
        $methods = array('getAssociatedContacts', 'getAssociatedOrganizations', 'getAssociatedDeals');
        foreach ($methods as $method) {
            if (method_exists($for, $method)) {
                $records = $for->$method();
                foreach ($records as $record){
                    $draftRecord->linkTo($record, 'requiresTask');
                }
            }
            
        }
        
        return $draftRecord;
    }
    
    /**
     * @param int           $id                   Task id
     * @param string|null   $itemId
     * @return \Widget_FlowLayout
     */
    public function _attendees($id, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems();
        $box->setHorizontalSpacing(2, 'em');
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $recordSet = $this->getRecordSet(true);
        $record = $recordSet->request($id);
        
        
        $box->addItem(
            $W->Link(
                $App->translate('Add attendee'),
                $this->proxy()->addAttendee($record->id)
            )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD, 'widget-actionbutton')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        $taskAttendees = $record->getAttendees();
        
        
        $participantsBox = $W->VBoxItems();
        $participantsBox->setVerticalSpacing(2, 'px');
        $box->addItem($participantsBox);
        
        foreach ($taskAttendees as $taskAttendee) {
            $attendee = $taskAttendee->attendee();
            
            $attendeeName = $attendee->getRecordTitle();
            
            $line = $W->FlowItems(
                $W->FlowItems(
                    $W->Link(
                        '',
                        $this->proxy()->changeAttendeeType($taskAttendee->id)
                    )->addClass($taskAttendee->getTypeIcon())->setTitle($taskAttendee->getTypeTitle())->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    $W->Html(bab_toHtml($attendeeName))
                )->setHorizontalSpacing(0.5, 'em')
                ->setSizePolicy('widget-80pc'),
                $W->FlowItems(
                    $W->Link(
                        '', $this->proxy()->removeAttendee($taskAttendee->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                )->addClass('widget-actions', 'widget-align-right')
                ->setSizePolicy('widget-20pc')
            );
            
            $line->addClass('widget-actions-target');
            
            $participantsBox->addItem($line);
        }
        
        
        $box->addClass(\Func_Icons::ICON_LEFT_16);
        
        $box->setReloadAction($this->proxy()->_attendees($record->id, $box->getId()));
        $box->addClass('depends-attendees');
        return $box;
    }
    
    /**
     * @param int $id       The task id
     * @return \app_Page
     */
    public function addAttendee($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $this->getRecordSet();
        $recordSet->setDefaultCriteria($recordSet->deleted->in(\app_TraceableRecord::DELETED_STATUS_EXISTING, \app_TraceableRecord::DELETED_STATUS_DRAFT));
        
        $record = $recordSet->request($id);
        
        $page = $Ui->Page();
        $page->setTitle($App->translate('Add attendee'));
        
        $editor = new \app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem(
            $W->Hidden()
            ->setName('id')
            ->setValue($id)
        );
        $editor->addItem(
            $App->Task()->Ui()->SuggestParticipant($record->id)
            ->setPlaceHolder($App->translate('Select a contact or team'))
            ->setName('newAttendee')
            ->setMinChars(0)
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Attendee type'),
                $W->Select()->setOptions(TaskAttendee::getTypes()),
                'attendeeType'
            )    
        );
        
        $editor->setSaveAction($this->proxy()->saveAttendee(), $App->translate('Add'));
        $editor->isAjax = bab_isAjaxRequest();
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function addMultiAttendee(){
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $selectedRows = $this->getModelViewSelectedRows();
        $Ui->includeTicket();
        
        $nbSelected = count($selectedRows);
        
        $page = $Ui->Page();
        $page->setTitle(sprintf($App->translate('Add attendee for %d task', 'Add attendee for %d tasks', $nbSelected), $nbSelected));
        
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem(
            $App->Task()->Ui()->SuggestParticipant()
            ->setPlaceHolder($App->translate('Select a contact or team'))
            ->setName('newAttendee')
            ->setMinChars(0)
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Attendee type'),
                $W->Select()->setOptions(TaskAttendee::getTypes()),
                'attendeeType'
            )
        );
        
        $editor->setSaveAction($this->proxy()->saveMultiAttendee());
        $editor->isAjax = true;
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveMultiAttendee($data = null){
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->TaskSet();
        $selectedRows = $this->getModelViewSelectedRows();
        
        $attendeeId = isset($data['newAttendee_ID']) && $data['newAttendee_ID'] !== '' ? $data['newAttendee_ID'] : null;
        $attendeeType = isset($data['attendeeType']) && $data['attendeeType'] !== '' ? $data['attendeeType'] : TaskAttendee::TYPE_MEMBER_OPTIONAL;
        
        if ($attendeeId) {
            $attendee = $App->getRecordByRef($attendeeId);
            $contactClassName = $App->ContactClassName();
            $attendeeIsContact = $attendee instanceof $contactClassName;
            foreach($selectedRows as $selectId){
                $record = $set->request($selectId);
                if ($attendeeIsContact) {
                    if(!$record->responsible){
                        $record->responsible = $attendee->id;
                    }
                    
                    $attendeeComponent = $App->getComponentByName('TaskAttendee');
                    $attendeeSet = $attendeeComponent->recordSet();
                    
                    $attendeeRef = $attendee->getRef();
                    $same = $attendeeSet->select($attendeeSet->task->is($record->id)->_AND_($attendeeSet->attendee->is($attendeeRef)));
                    
                    if($same->count() == 0){
                        //Dont add attendee again if already on task
                        $newAttendee = $attendeeSet->newRecord();
                        $newAttendee->task = $record->id;
                        $newAttendee->attendee = $attendee->getRef();
                        $newAttendee->type = $attendeeType;
                        $newAttendee->save();
                    }
                }
            }
            
            $this->addReloadSelector('.depends-Task');
            $this->addReloadSelector('.depends-attendees');
        }
        
        return true;
    }
    
    /**
     *
     * @param array $data
     */
    public function saveAttendee($data = null)
    {
        $App = $this->App();
        $set = $this->getRecordSet(true);
        
        $record = $set->request($data['id']);
        
        $attendeeId = isset($data['newAttendee_ID']) && $data['newAttendee_ID'] !== '' ? $data['newAttendee_ID'] : null;
        $attendeeType = isset($data['attendeeType']) && $data['attendeeType'] !== '' ? $data['attendeeType'] : TaskAttendee::TYPE_MEMBER_OPTIONAL;
        
        if ($attendeeId) {
            $attendee = $App->getRecordByRef($attendeeId);
            /**
             * @todo Use Contact component
             */
            $contactClassName = $App->ContactClassName();
            if ($attendee instanceof $contactClassName) {
                if(!$record->responsible){
                    $record->responsible = $attendee->id;
                }
//                 $record->linkTo($attendee);
//                 $this->addReloadSelector('.reload_linkedElements');
            }
            
            $attendeeComponent = $App->getComponentByName('TaskAttendee');
            $attendeeSet = $attendeeComponent->recordSet();
            
            $newAttendee = $attendeeSet->newRecord();
            $newAttendee->task = $record->id;
            $newAttendee->attendee = $attendee->getRef();
            $newAttendee->type = $attendeeType;
            $newAttendee->save();
            
            $this->addReloadSelector('.depends-attendees');
        }
        
        return true;
    }
    
    public function saveAttendeeType($data = null)
    {
        $App = $this->App();
        $attendeeSet = $App->TaskAttendeeSet();
        
        $attendee = $attendeeSet->request($data['id']);
        $attendee->type = $data['attendeeType'];
        $attendee->save();
        
        $this->addReloadSelector('.depends-attendees');
        return true;
    }
    
    /**
     *
     * @param int $id                   TaskAttendee id
     * @return boolean
     */
    public function removeAttendee($id)
    {
        $App = $this->App();
        $set = $App->TaskAttendeeSet();
        
        $record = $set->request($id);
        $record->delete();
        
        $this->addReloadSelector('.depends-attendees');
        
        return true;
    }
    
    /**
     * @param int $id       The taskAttendee id
     * @return \app_Page
     */
    public function changeAttendeeType($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        $recordSet = $App->TaskAttendeeSet();
        $record = $recordSet->request($id);
        
        $page = $Ui->Page();
        $page->setTitle($App->translate('Change attendee type'));
        
        $editor = new \app_Editor($App);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem(
            $W->Hidden()
            ->setName('id')
            ->setValue($id)
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Attendee type'),
                $W->Select()->setOptions(TaskAttendee::getTypes())->setValue($record->type),
                'attendeeType'
            )
        );
        
        $editor->setSaveAction($this->proxy()->saveAttendeeType(), $App->translate('Save'));
        $editor->isAjax = bab_isAjaxRequest();
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Add a task
     * @param string $for       A App element reference
     * @return \app_Page
     */
    public function add($for = null, $type = null, $draft = null, $reloaded = false, $id = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        /**
         * @var $set \app_TraceableRecordSet
         */
        $set = $this->getRecordSet(true);
        if (! $reloaded) {
            $draftRecord = $this->createDraftRecord($for, $type);
        } else {
            $draftRecord = $set->get($draft);
        }
        
        $infoEditor = $this->getInfoEditor($draftRecord->id);
        $linkedElementsEditor = $this->getLinkedElementsEditor($draftRecord->id);
        $sectionEditor = $this->getSectionEditor($draftRecord->id);
        
        $editors = $W->VBoxItems(
            $infoEditor,
            $linkedElementsEditor,
            $sectionEditor
        );
        
        if ($reloaded) {
            return $editors;
        }
        
        $sectionEditor->setSaveAction($this->proxy()->save());
        
        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Create a new task'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    /**
     * Add a task
     * @param string $for       A App element reference
     * @return \app_Page
     */
    public function addMilestone($for = null, $type = null, $draft = null, $reloaded = false, $id = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        /**
         * @var $set \app_TraceableRecordSet
         */
        $set = $this->getRecordSet(true);
        if (! $reloaded) {
            $draftRecord = $this->createDraftRecord($for, $type);
        } else {
            $draftRecord = $set->get($draft);
        }
        
        $infoEditor = $this->getInfoEditor($draftRecord->id);
        $linkedElementsEditor = $this->getLinkedElementsEditor($draftRecord->id);
        $sectionEditor = $this->getSectionEditor($draftRecord->id);
        
        $editors = $W->VBoxItems(
            $infoEditor,
            $linkedElementsEditor,
            $sectionEditor
        );
        
        if ($reloaded) {
            return $editors;
        }
        
        $sectionEditor->setSaveAction($this->proxy()->save());
        
        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Create a new task'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    public function getLinkedElementsEditor($task, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $taskSet = $this->getRecordSet(true);
        
        $task = $taskSet->request($taskSet->id->is($task));
        
        $editor = new \app_Editor($App);
        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        $editor->setRecord($task);
        $editor->setHiddenValue('data[id]',$task->id);
        $editor->isAjax = bab_isAjaxRequest();
        $editor->recordSet = $taskSet;
        
        /**
         * LINKED ELEMENTS
         */
        
        $linkedElementsSection = $W->Section(
            $App->translate('Linked elements'),
            $linkedElementsContent = $W->VBoxItems()
        )->addClass('box magenta');
        
        
        $table = $W->TableArrayView(
            array(
                $App->translate('Contacts'),
                $App->translate('Organizations'),
                $App->translate('Deals')
            )
        )->setIconFormat(16, 'left');
        
        $table->setColumnClasses(array(1 => array('col-md-4'), 2 => array('col-md-4'), 3 => array('col-md-4')));
        
        $Ui = $App->Ui();
        
        $content = array();
        
        $content[] = array(
            $W->LabelledWidget(
                $App->translate('Add contact'),
                $Ui->SuggestContact(),
                'contact'
            )->setSizePolicy('col-md-4'),
            $W->LabelledWidget(
                $App->translate('Add organization'),
                $Ui->SuggestOrganization(),
                'organization'
            )->setSizePolicy('col-md-4'),
            $W->LabelledWidget(
                $App->translate('Add deal'),
                $Ui->SuggestDeals(),
                'deal'
            )->setSizePolicy('col-md-4')
        );
        
        $links = $task->getLinks();
        $nbLinks = $links->count();
        
        $contactCol = array();
        $organizationCol = array();
        $dealCol = array();
        
        foreach ($links as $link){
            $source = $link->getSource();
            $sourceClassname = $source->getClassName();
            
            $cell = $W->FlowItems(
                $cellContent = $W->FlowItems()->setHorizontalSpacing(0.5, 'em')
            )->setSizePolicy('col-md-4');
            
            $linkContent = $W->FlowItems()->addClass('widget-actions', 'widget-align-right');
            
            if ($nbLinks > 1) {
                $linkContent->addItem(
                    $W->Link(
                        '', $this->proxy()->confirmDeleteTaskLink($link->id)
                    )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setAjaxAction()
                );
                $cell->addClass('widget-actions-target');
            }
            
            switch ($sourceClassname) {
                case 'Contact':
                    $contactContent = $W->HBoxItems()->setSpacing(1, 'em')->setHorizontalSpacing(1, 'em');
                    if ($task->responsible != $source->id) {
                        $contactContent->addItem(
                            $W->Link(
                                '', $this->proxy()->makeResponsible($task->id, $source->id)
                            )->addClass('icon actions-list-add-user')->setTitle($App->translate('Make responsible'))
                            ->setAjaxAction()
                        );
                    } else {
                        $contactContent->addItem(
                            $W->Label('')->addClass('icon apps-users-o')->setTitle($App->translate('Responsible'))
                        );
                    }
                    $contactContent->addItems(
                        $W->Link(
                            $Ui->ContactAvatar($source, 20, 20)
                                ->setSizePolicy('minimum'),
                            $App->Controller()->Contact()->display($source->id)
                        ),
                        $W->VBoxItems(
                            $W->VBoxItems(
                                $W->Link(
                                    $source->getFullName(),
                                    $App->Controller()->Contact()->display($source->id)
                                )->addClass('crm-card-title', 'widget-strong')
                            )
                        )->setVerticalSpacing(0.5, 'em')->setSizePolicy(\Widget_SizePolicy::MAXIMUM),
                        $linkContent
                    );
                    $cellContent->addItem($contactContent);
                    $contactCol[] = $cell;
                    break;
                case 'Organization':
                    $cellContent->addItem(
                        $W->HBoxItems(
                            $W->Link(
                                $App->Ui()->OrganizationLogo($source, 28, 28)->setSizePolicy(\Widget_SizePolicy::MINIMUM),
                                $App->Controller()->Organization()->display($source->id)
                            ),
                            $W->VBoxItems(
                                $W->VBoxItems(
                                    $W->Link(
                                        $source->name,
                                        $App->Controller()->Organization()->display($source->id)
                                    )->addClass('crm-card-title', 'widget-strong')
                                )
                            )->setVerticalSpacing(0.5, 'em')->setSizePolicy(\Widget_SizePolicy::MAXIMUM),
                            $linkContent
                        )->setSpacing(1, 'em')->setHorizontalSpacing(1, 'em')
                    );
                    $organizationCol[] = $cell;
                    break;
                case 'Deal':
                    $cellContent->addItem(
                        $W->HBoxItems(
                            $W->Frame()->setCanvasOptions(
                                \Widget_Item::Options()->backgroundColor('#' . $source->status()->color)
                            )->setSizePolicy(\Widget_SizePolicy::MINIMUM)->addClass('crm-color-preview'),
                            $W->Link($W->Title($source->name, 6), $App->Controller()->Deal()->display($source->id)),
                            $linkContent
                        )->setSizePolicy(\Widget_SizePolicy::MAXIMUM)->setVerticalAlign('middle')->addClass(\Func_Icons::ICON_LEFT_16)->setSizePolicy('widget-list-element')->setSpacing(1, 'em')->setHorizontalSpacing(1, 'em')
                    );
                    $dealCol[] = $cell;
                    break;
            }
        }
        
        $nbContact = count($contactCol);
        $nbOrganization = count($organizationCol);
        $nbDeal = count($dealCol);
        
        $nbCell = max($nbContact, $nbOrganization, $nbDeal);
        $emptyCell = $W->FlowItems()->setSizePolicy('col-md-4');
        for ($i = 0; $i < $nbCell; $i ++) {
            $row = array();
            
            $row[] = isset($contactCol[$i]) ? $contactCol[$i] : $emptyCell;
            $row[] = isset($organizationCol[$i]) ? $organizationCol[$i] : $emptyCell;
            $row[] = isset($dealCol[$i]) ? $dealCol[$i] : $emptyCell;
            
            $content[] = $row;
        }
        
        $table->setContent($content);
        $table->addRowClass(1, 'bg-info');
        $linkedElementsContent->addItem($table);
        
        $editor->addItem($linkedElementsSection);
        $editor->setAjaxAction($this->proxy()->saveLinkedElements(), '', 'change');
        $editor->setReloadAction($this->proxy()->getLinkedElementsEditor($task->id, $editor->getId()));
        $editor->addClass('reload_linkedElements');
        $editor->addClass('widget-no-close');
        
        return $editor;
    }
    
    public function makeResponsible($taskId, $contactId)
    {
        $App = $this->App();
        
        $taskSet = $this->getRecordSet(true);
        $task = $taskSet->request($taskSet->id->is($taskId));
        
        $contactSet = $App->ContactSet();
        $contact = $contactSet->request($contactSet->id->is($contactId));
        
        $task->responsible = $contact->id;
        $task->save();
        
        $this->addReloadSelector('.reload_linkedElements');
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        
        return true;
    }

	public function saveLinkedElements($data = array())
    {
        $this->requireSaveMethod();
        $App = $this->App();
        
        $taskSet = $this->getRecordSet(true);
        /* @var $task Task */
        $task = $taskSet->get($taskSet->id->is($data['id']));
        if ($task) {
            if (isset($data['contact_ID']) && ! empty($data['contact_ID'])) {
                $set = $App->ContactSet();
                $record = $set->get($set->id->is($data['contact_ID']));
                if ($record) {
                    $task->linkTo($record);
                    $subjectSet = $record->getParentSet();
                    if($subjectSet && method_exists($subjectSet, 'getAscendantKeys')){
                        $ascendantKeys = $subjectSet->getAscendantKeys();
                        foreach ($ascendantKeys as $ascendantKey){
                            if($ascendant = $record->$ascendantKey()){
                                if (!$task->isLinkedTo($ascendant)) {
                                    $task->linkTo($ascendant,'requiresTask_ascendant');
                                }
                            }
                        }
                    }
                }
            }
            if (isset($data['organization_ID']) && ! empty($data['organization_ID'])) {
                $set = $App->OrganizationSet();
                $record = $set->get($set->id->is($data['organization_ID']));
                if ($record) {
                    $task->linkTo($record);
                    $subjectSet = $record->getParentSet();
                    if($subjectSet && method_exists($subjectSet, 'getAscendantKeys')){
                        $ascendantKeys = $subjectSet->getAscendantKeys();
                        foreach ($ascendantKeys as $ascendantKey){
                            if($ascendant = $record->$ascendantKey()){
                                if (!$task->isLinkedTo($ascendant)) {
                                    $task->linkTo($ascendant,'requiresTask_ascendant');
                                }
                            }
                        }
                    }
                }
            }
            if (isset($data['deal_ID']) && ! empty($data['deal_ID'])) {
                $set = $App->DealSet();
                $record = $set->get($set->id->is($data['deal_ID']));
                if ($record) {
                    $task->linkTo($record);
                    $subjectSet = $record->getParentSet();
                    if($subjectSet && method_exists($subjectSet, 'getAscendantKeys')){
                        $ascendantKeys = $subjectSet->getAscendantKeys();
                        foreach ($ascendantKeys as $ascendantKey){
                            if($ascendant = $record->$ascendantKey()){
                                if (!$task->isLinkedTo($ascendant)) {
                                    $task->linkTo($ascendant,'requiresTask_ascendant');
                                }
                            }
                        }
                    }
                }
            }
        }
        
        $this->addReloadSelector('.reload_linkedElements');
        return true;
    }
    
    public function getSectionEditor($task, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $taskSet = $this->getRecordSet(true);
        
        $task = $taskSet->request($taskSet->id->is($task));
        
        $sectionEditor = $App->Task()->Ui()->SectionEditor($task, $itemId);
        
        $sectionEditor->setName('data');
        $sectionEditor->setRecord($task);
        $sectionEditor->setHiddenValue('data[id]',$task->id);
        $sectionEditor->isAjax = bab_isAjaxRequest();
        
        $sectionEditor->recordSet = $taskSet;
        
        $customSectionSet = $App->CustomSectionSet();
        $customSections = $customSectionSet->select($customSectionSet->view->is($task->getBlueprint())->_AND_($customSectionSet->object->is('Task')));
        $customSections->orderAsc($customSectionSet->rank);
        
        foreach ($customSections as $customSection){
            $section = $W->Section(
                $customSection->name,
                $sectionEditor->sectionContent($customSection->id)
            );
            $section->addClass($customSection->classname);
            $section->setSizePolicy($customSection->sizePolicy);
            $section->setFoldable($customSection->foldable, $customSection->folded);
            $sectionEditor->addItem($section);
        }
        
        $sectionEditor->setAjaxAction($this->proxy()->saveChange(), '', 'change input', 5000);
        $sectionEditor->setReloadAction($this->proxy()->getSectionEditor($task->id, $sectionEditor->getId()));
        $sectionEditor->addClass('reload_task_section_editor');
        $sectionEditor->addClass('widget-no-close');
        $sectionEditor->addClass('row');
        
        return $sectionEditor;
    }
    
    
    
    /**
     * 
     * @param int $id       The task id
     * @param boolean $ajax
     * @param string $itemId
     * @return \app_Editor
     */
    public function getInfoEditor($id, $ajax = true, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $set = $this->getRecordSet(true);
        $task = $set->request($id);
        
        /* @var $task Task */
        
        $editor = new \app_Editor($App, $itemId);
        $editor->setIconFormat(16, 'left');
        $editor->setName('data');
        $editor->isAjax = true;
        
        $editor->addItem($W->Hidden()->setName('id'));
        
        /**
         * MAIN
         */
//         $mainSection = $W->Section(
//             $App->translate('Task'),
            $mainContent = $W->VBoxItems()->setVerticalSpacing(1, 'em');
//         )->addClass('box');
        
        //TITLE + PRIVATE + MILESTONE
        $mainContent->addItem(
            $W->HBoxItems(
                $W->LabelledWidget(
                    $App->translate('Title'),
                    $W->LineEdit()->setSize(52)->setMaxSize(255)
                    ->setMandatory(true, $App->translate('The title must not be empty.')),
                    'summary'
                )->setSizePolicy('maximum'),
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Html($App->translate('Private')),
                        $W->CheckBox()
                        ->setName('private')
                    )->setHorizontalSpacing(1, 'em')
                    ->addClass('widget-nowrap', 'icon', \Func_Icons::ACTIONS_AUTHENTICATE),
                    $W->FlowItems(
                        $W->Html($App->translate('Milestone')),
                        $W->CheckBox()
                        ->setName('milestone')
                    )->setHorizontalSpacing(1, 'em')
                    ->addClass('widget-nowrap', 'icon', 'objects-flag')
                )->addClass('widget-align-right'),
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Html($App->translate('Is billable')),
                        $W->CheckBox()
                            ->setName('isBillable')
                    )->setHorizontalSpacing(1, 'em')
                    ->addClass('widget-nowrap', 'icon', \Func_Icons::OBJECTS_INVOICE),
                    $W->FlowItems(
                        $W->Html($App->translate('CIR')),
                        $W->CheckBox()
                            ->setName('cir')
                    )->setHorizontalSpacing(1, 'em')
                    ->addClass('widget-nowrap', 'icon', \Func_Icons::OBJECTS_CONTACT)
                )->addClass('widget-align-right')
                ->setSizePolicy('minimum')
            )->addClass('widget-100pc')
            ->setHorizontalSpacing(2, 'em')
        );
        
        //TYPE
        $taskTypeSet = $App->TaskTypeSet();
        $taskTypes = $taskTypeSet->select();
        
        $select = $W->Select();
        $select->addOption(0, '');
        
        foreach ($taskTypes as $taskType) {
            $select->addOption($taskType->id, $taskType->name);
        }
        
        $mainContent->addItem(
            $W->LabelledWidget(
                $App->translate('Type'),
                $select,
                'type'
            )
        );
        
        //DATE
        $scheduledStartTime = explode(' ', $task->scheduledStart);
        $scheduledFinishTime = explode(' ', $task->scheduledFinish);
        
        $dateItem = $W->DatePicker();
        $startDateItem = $W->DatePicker();
        $startTimeItem = $W->TimeEdit();
        $endTimeItem = $W->TimeEdit();
        
        $whenFormItem = $W->VBoxItems(
            $W->Label($App->translate('When'))->addClass('widget-description'),
            $W->FlowItems(
                $dateItem->setName('dueDate')
//                 $startTimeItem->setName('scheduledStartTime')->setValue($scheduledStartTime[1]),
//                 $endTimeItem->setName('scheduledFinishTime')->setValue($scheduledFinishTime[1])
            )->setHorizontalSpacing(2, 'em')
        );
        
        $sendFormItem = $W->LabelledWidget(
            $App->translate('Add to attendees calendars'),
            $W->CheckBox(),
            'send'
        );
        
        $mainContent->addItem(
            $W->FlowItems(
                $whenFormItem
//                 $sendFormItem
            )->setHorizontalSpacing(3, 'em')
        );
        
        $scheduleSection = $W->Section(
            $App->translate('Schedule'),
            $W->VBoxItems(
                $W->LabelledWidget(
                    $App->translate('Scheduled start'),
                    $W->FlowItems(
                        $startDateItem->setName('scheduledStartDate')->setValue($scheduledStartTime[0]),
                        $startTimeItem->setName('scheduledStartTime')->setValue($scheduledStartTime[1]),
                        $endTimeItem->setName('scheduledFinishTime')->setValue($scheduledFinishTime[1]),
                        $sendFormItem
                    )->setHorizontalSpacing(2, 'em')
                ),
                $W->FlowItems(
                    $W->LabelledWidget(
                        $App->translate('Planned work'),
                        $W->LineEdit(),
                        'work',
                        null,
                        'H'
                    ),
                    $W->LabelledWidget(
                        $App->translate('Actual work'),
                        $W->LineEdit(),
                        'actualWork',
                        null,
                        'H'
                    ),
                    $W->LabelledWidget(
                        $App->translate('Remaining work'),
                        $W->LineEdit(),
                        'remainingWork',
                        null,
                        'H'
                    ),
                    $W->LabelledWidget(
                        $App->translate('Completion'),
                        $W->NumberEdit()
                            ->setStep(5)
                            ->setMin(0)
                            ->setMax(100)
                            ->addAttribute('type', 'range'),
                        'completion',
                        null,
                        '%'
                    )
                )->setHorizontalSpacing(2, 'em')
                ->setVerticalAlign('top')
            )->setVerticalSpacing(1, 'em')
        )->setFoldable(true)
        ->addClass('box');
        
        $mainContent->addItem($scheduleSection);
        
        //COMPLETION
//         $mainContent->addItem(
//             $W->LabelledWidget(
//                 $App->translate('Completion'),
//                 $W->NumberEdit()
//                 ->setStep(5)
//                 ->setMin(0)
//                 ->setMax(100)
//                 ->addAttribute('type', 'range'),
//                 'completion',
//                 null,
//                 '%'
//             )
//         );
        
        /**
         * ATTENDEES
         */
        $attendeesSection = $W->Section(
            $App->translate('Attendees'),
            $attendeesContent = $W->VBoxItems()
        )->addClass('box blue');
        
        //ATTENDEES
        $attendeesContent->addItem(
            $W->LabelledWidget(
                $App->translate('Attendees'),
                $W->VBoxItems(
                    $this->_attendees($task->id)
                )
            )
        );
        
        //ALARMS
        $attendeesContent->addItem(
            $W->LabelledWidget(
                $App->translate('Notifications'),
                $W->VBoxItems(
                    $W->Link(
                        $App->translate('Add a notification'),
                        $App->Controller()->Alarm()->add($task->id)
                    )->addClass('widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                    ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
                    ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                    $App->Controller()->Alarm(false)->listFor($task->id)
                )
            )
        );
        
        /**
         * INFO SECTIONS
         */
        $editor->addItem($mainContent);
        $editor->addItem($attendeesSection);
        
        $editor->setRecord($task);
        $editor->setReloadAction($this->proxy()->getInfoEditor($task->id, $ajax, $editor->getId()));
        if ($ajax) {
            $editor->setAjaxAction($this->proxy()->saveChange(), null, 'change');
        }
        $editor->addClass('depends-' . $this->getRecordClassName());
        $editor->addClass('widget-no-close');
        return $editor;
    }
    
    /**
     * Add an extended task
     * @param string $for       A Crm element reference
     * @return \app_Page
     */
    public function edit($id = null, $reloaded = false, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $page = $App->Ui()->Page();
        
        $set = $this->getRecordSet(true);
        $task = $set->request($id);
        
        $page->setTitle($App->translate('Edit task'));
        
        $infoEditor = $this->getInfoEditor($task->id);
        $linkedElementsEditor = $this->getLinkedElementsEditor($task->id);
        $sectionEditor = $this->getSectionEditor($task->id);
        
        $editors = $W->VBoxItems(
            $infoEditor,
            $linkedElementsEditor,
            $sectionEditor
        );
        
        if ($reloaded) {
            return $editors;
        }
        
        $sectionEditor->setSaveAction($this->proxy()->save());
        
        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Edit task'));
        $page->addClass('app-page-editor');
        $page->addClass('widget-no-close');
        
        $page->addItem($editors);
        return $page;
    }
    
    public function save($data = null)
    {
        $attendeeId = null;
        if (isset($data['newAttendee_ID']) && $data['newAttendee_ID'] !== '') {
            $attendeeId = $data['newAttendee_ID'];
        }
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        return $this->saveAction($data, $attendeeId, true);
    }
    
    /**
     * Saves the task.
     *
     * @param array	$task
     * @return \Widget_Action
     */
    public function saveChange($data = null)
    {
        $attendeeId = null;
        if (isset($data['newAttendee_ID']) && $data['newAttendee_ID'] !== '') {
            $attendeeId = $data['newAttendee_ID'];
        }
        
        return $this->saveAction($data, $attendeeId, false);
    }    
    
    public function saveAction($data = null, $newAttendee_ID = null, $finalSave = false)
    {
        $App = $this->App();
        $set = $this->getRecordSet(true);
        
        if (isset($data['scheduledStartTime']) && isset($data['scheduledFinishTime'])) {
            $data['scheduledStart'] = app_fixDateTime(array('date' => $data['scheduledStartDate'], 'time' => $data['scheduledStartTime']), $data['scheduledAllDay']);
            $data['scheduledFinish'] = app_fixDateTime(array('date' => $data['scheduledStartDate'], 'time' => $data['scheduledFinishTime']), $data['scheduledAllDay']);
            
            if (empty($data['scheduledFinishTime']) || $data['scheduledFinishTime'] == '00:00' || $data['scheduledStart'] > $data['scheduledFinish']) {
                $finish = \BAB_DateTime::fromIsoDateTime($data['scheduledStart']);
                $finish->add(1, \BAB_DATETIME_HOUR);
                $data['scheduledFinish'] = $finish->getIsoDateTime();
            }
        }
        
        if (isset($data['id'])) {
            $record = $set->request($data['id']);
        } else {
            $record = $set->newRecord();
        }
        
        /* @var $record Task */
        
        $attendeeId = null;
        if (isset($newAttendee_ID) && $newAttendee_ID !== '') {
            $attendeeId = $newAttendee_ID;
        }
        
        $originalType = $record->type;
        $record->setFormInputValues($data);
        

        
        if ($finalSave) {
            $record->deleted = \app_TraceableRecord::DELETED_STATUS_EXISTING;
        }
        $record->save();
        
        
        if ($attendeeId) {
            $attendee = $App->getRecordByRef($attendeeId);
            /**
             * @todo Use Contact component
             */
            $contactClassName = $App->ContactClassName();
            if ($attendee instanceof $contactClassName && !$record->responsible) {
                $record->responsible = $attendee->id;
            }
            
            $attendeeComponent = $App->getComponentByName('TaskAttendee');
            $attendeeSet = $attendeeComponent->recordSet();
            
            $newAttendee = $attendeeSet->newRecord();
            $newAttendee->task = $record->id;
            $newAttendee->attendee = $attendee->getRef();
            $newAttendee->save();
            
            
            $this->addReloadSelector('.depends-attendees');
        }
        
        if (isset($data['for']) && $data['for'] !== '') {
            $references = explode(',', $data['for']);
            foreach ($references as $reference) {
                $subject = $App->getRecordByRef($reference);
                if (!$record->isLinkedTo($subject)) {
                    $record->linkTo($subject);
                }
                $subjectSet = $subject->getParentSet();
                if($subjectSet && method_exists($subjectSet, 'getAscendantKeys')){
                    $ascendantKeys = $subjectSet->getAscendantKeys();
                    foreach ($ascendantKeys as $ascendantKey){
                        if($ascendant = $subject->$ascendantKey()){
                            if (!$record->isLinkedTo($ascendant)) {
                                $record->linkTo($ascendant,'requiresTask_ascendant');
                            }
                        }
                    }
                }
            }
        }
        
        if ($finalSave && $record->send) {
            $record->sendNotification();
        }
        
        if ($record->type != $originalType) {
            $this->addReloadSelector('.reload_task_section_editor');
        }
        
        return true;
    }  
    
    
    /**
     * Displays a form to edit a clone of the specified task.
     *
     * @param int    $task The task id or null for a new task.
     * @return \app_Page
     */
    public function duplicate($task = null, $reloaded = false, $id = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        if (!isset($task)) {
            throw new \app_Exception('Trying to access task with wrong (unknown) id.');
        }
        
        /**
         * @var TaskSet $taskSet
         * @var Task    $task
         */
        $taskSet = $this->getRecordSet(true);
        $task = $taskSet->get($task);
        if (!isset($task)) {
            throw new \app_Exception('Trying to access task with wrong (unknown) id.');
        }
        
        $page->addItem($W->Title($App->translate('Duplicate task'), 1));
        
        
        $linkedObjectsFrame = $App->Task()->Ui()->LinkedObjects($task);
        
        $page->addContextItem($linkedObjectsFrame);
        
        if(!$reloaded){
            $linkedObjectsReferences = array();
            $contacts = $task->getLinkedContacts();
            foreach ($contacts as $contact) {
                $linkedObjectsReferences[$contact->getRef()] = $contact->getRef();
            }
            $deals = $task->getLinkedDeals();
            foreach ($deals as $deal) {
                $linkedObjectsReferences[$deal->getRef()] = $deal->getRef();
            }
            $organizations = $task->getLinkedOrganizations();
            foreach ($organizations as $organization) {
                $linkedObjectsReferences[$organization->getRef()] = $organization->getRef();
            }
            
            $draft = $taskSet->newRecord();
            $draft->setValues($task->getValues());
            
            $draft->deleted = \app_TraceableRecord::DELETED_STATUS_DRAFT;
            $draft->id = null;
            $draft->uuid = null;
            
            $draft->save();
            
            $linkSet = $App->LinkSet();
            $links = $linkSet->select(
                $linkSet->targetClass->is(get_class($task))
                ->_AND_($linkSet->targetId->is($task->id))
                ->_AND_($linkSet->type->is('requiresTask'))
            );
            
            foreach ($links as $link){
                $source = $link->getSource();
                $linkedObjectsReferences[$source->getRef()] = $source->getRef();
            }
        }
        else{
            $draft = $task;
        }
        /**
         * @var $editor TaskEditor
         */
        $editor = $App->Task()->Ui()->TaskEditor($draft, $reloaded, $id);
        
        $editor->setName('data');
        $editor->setRecord($draft);
        $editor->setHiddenValue('data[id]',$draft->id);
        $editor->isAjax = bab_isAjaxRequest();
        
        if (!empty($linkedObjectsReferences)) {
            $editor->setHiddenValue('data[for]',implode(',', $linkedObjectsReferences));
        }
        
        $editor->setAjaxAction($this->proxy()->saveChange(), null, 'change');
        
        $editor->setReloadAction($this->proxy()->duplicate($draft->id, true, $editor->getId()));
        $editor->addClass('depends-' . $this->getRecordClassName());
        $editor->addClass('widget-no-close');
        
        if($reloaded){
            return $editor;
        }
        
        $page->addItem($editor);
        return $page;
    }
    
    
    
    /**
     * Actions frame for task display
     * @param Task $task
     * @return \Widget_VBoxLayout
     */
    protected function getActionsFrame(Task $task)
    {
        $App = $this->App();
//         $Access = $Crm->Access();
        $W = bab_Widgets();
        $actionsFrame = $W->VBoxLayout()
        ->setVerticalSpacing(1, 'px')
        ->addClass(\Func_Icons::ICON_LEFT_16)
        ->addClass('app-actions');
        
        
//         if ($Access->updateTask($task->id)) {
//             $actionsFrame->addItem(
//                 $W->Link(
//                     $W->Icon(
//                         $App->translate('Edit this task'), 
//                         \Func_Icons::ACTIONS_DOCUMENT_EDIT
//                     ), 
//                     $this->proxy()->edit($task->id)
//                 )
//             );
//         }
        
        $actionsFrame->addItem(
            $W->Link(
                $W->Icon(
                    $App->translate('Duplicate this task'),
                    \Func_Icons::ACTIONS_EDIT_COPY
                ), 
                $this->proxy()->duplicate($task->id)
            )->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
//         if ($Access->deleteTask($task->id)) {
//             $actionsFrame->addItem(
//                 $W->Link(
//                     $W->Icon(
//                         $App->translate('Delete this task'),
//                         \Func_Icons::ACTIONS_EDIT_DELETE
//                     ),
//                     $this->proxy()->confirmDelete($task->id)
//                 )->setOpenMode(\Widget_Link::OPEN_DIALOG)
//             );
//         }
        
        return $actionsFrame;
    }
    
    /**
     *
     * @param int $task 		The task id
     * @return \Widget_Action
     */
    public function display($id, $view = '', $itemId = null)
    {
        $App = $this->App();
        $recordSet = $this->getRecordSet();
        
        $record = $recordSet->get($id);
        if(!isset($record)){
            return true;
        }
        
        if (!$record->isReadable()) {
            throw new \app_AccessException($App->translate('You do not have access to this page.'));
        }
        
        $fullFrame = $App->Task()->Ui()->FullFrame($record);
        if(!isset($view) || empty($view)){
            $view = $record->getBlueprint();
        }
        $fullFrame->setView($view);
        $fullFrame->setSizePolicy('widget-min50em');
        
        $page = $App->Ui()->Page();
        $page->setIconFormat(16, 'left');
        if (isset($itemId)) {
            $page->setId($itemId);
        }
        
        if($record->isDeletable())
        {
            $W = bab_Widgets();
            $page->addItem(
                $W->Link(
                    $App->translate('Delete this task'),
                    $this->proxy()->confirmDelete($id)
                )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                ->setSizePolicy('pull-right')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }
        
        $page->addItem($fullFrame);
        
        $page->addClass('depends-' . $this->getRecordClassName());
        $page->setReloadAction($this->proxy()->display($id), $page->getId());
        return $page;
    }
    
    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param int $customSectionId      The id of the section used to create the editor
     * @param string $itemId
     *
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editSection($id, $customSectionId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $page = $App->Ui()->Page();
        
        $customSectionSet = $App->CustomSectionSet();
        $customSection = $customSectionSet->get(
            $customSectionSet->id->is($customSectionId)
        );
        
        $page->setTitle($customSection->name);
        
        $recordSet = $this->getEditRecordSet();
        $record = $recordSet->request($id);
        if (!$record->isUpdatable()) {
            throw new \app_AccessException($App->translate('You do not have access to this page.'));
        }
        
        /* @var $editor TaskSectionEditor */
        $editor = $App->Task()->Ui()->SectionEditor($record);
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $page->setId($itemId);
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        
        $editor->setAjaxAction($this->proxy()->save(), '', 'save');
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));
        
        $editor->recordSet = $recordSet;
        $section = $editor->sectionContent($customSectionId);
        $section->setSizePolicy('widget-60em');
        
        $editor->addItem($section);
        $editor->setRecord($record);
        
        $page->addClass('widget-no-close');
        $page->setReloadAction($this->proxy()->editSection($id, $customSectionId, $page->getId()));
        
        $page->addItem($editor);
        
        return $page;
    }
    
    /**
     * Displays an editor of the record values corresponding to the fields of a custom section.
     *
     * @param int $id                   The id of the record to edit
     * @param string $itemId
     *
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function editTaskSection($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $page = $App->Ui()->Page();
        
        $page->setTitle($App->translate('Task'));
        
        $recordSet = $this->getEditRecordSet();
        $record = $recordSet->request($id);
        if (!$record->isUpdatable()) {
            throw new \app_AccessException($App->translate('You do not have access to this page.'));
        }
        
        if (!isset($itemId)) {
            $itemId = $this->getClass() . '_' . __FUNCTION__;
        }
        $editor = $this->getInfoEditor($record->id, false);
        $page->setId($itemId);
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->save());
        $editor->setName('data');
        $editor->addItem($W->Hidden()->setName('id'));
        
        $editor->recordSet = $recordSet;
        $editor->setRecord($record);
        
        $page->addClass('widget-no-close');
        $page->setReloadAction($this->proxy()->editTaskSection($id, $page->getId()));
        
        $page->addItem($editor);
        
        return $page;
    }
    
    
    /**
     * @return \Widget_Displayable_Interface
     */
    protected function getCompleteWidget(Task $task)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $editor = new \app_Editor($App);
        $editor->addItem(
            $editor->labelledField(
                $App->translate('Comment'),
                $W->TextEdit()->addClass('widget-fullwidth'),
                'comment'
            )
        );
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('task', $task->id);
        
        $editor->setSaveAction($this->proxy()->complete());
        $completeButton = $W->Accordions()->addClass('crm-action-accordion');
        $completeButton->addPanel($App->translate('I completed this task'), $editor);
        
        return $completeButton;
    }
    
    
    protected function completeWithWork(Task $task)
    {
        $message = $this->App()->translate('Please fill the actual work');
        throw new \app_SaveException($message);
    }
    
    
    
    /**
     * Updates the completion status of a task.
     *
     * @param int $task       The task id.
     * @param int $percent    The new completion rate (defaults to 100 which marks the task as completed
     *                        and sets the completedOn field to now()).
     * @param string $comment An optional comment text that will be appended to the task description.
     * @return \Widget_Action
     */
    public function complete($task = null, $completion = 100, $comment = null)
    {
        $App = $this->App();
        
        $taskSet = $this->getRecordSet();
        $task = $taskSet->request($task);
        
        if ($task->isPlanned()) {
            return $this->completeWithWork($task);
        }
        
        
        
        if ($task->completion < 100) {
            
            $task->completion = $completion;
            $task->remainingWork = 0;
            
            if (isset($comment)) {
                $task->description .= "\n\n" . $comment;
            }
            
            $task->save();
            
            if ($task->completion >= 100 && !empty($task->completedAction)) {
                app_redirect($task->completedAction);
            }
        }
        
        $this->addMessage($App->translate('The task has been saved.'));
        return true;
    }    
    
    /**
     *
     * @param array $tasks  array(taskId => '0' or '1')
     */
    public function markDone($tasks = null)
    {
        $App = $this->App();
//         $Access = $Crm->Access();
        
        $recordSet = $this->getRecordSet();
        if (isset($tasks) && !is_array($tasks)) {
            $tasks = array($tasks => true);
        }
        
        foreach ($tasks as $taskId => $checked) {
            $record = $recordSet->get($taskId);
            
//             if (!$Access->administer() && !$record->isUpdatable() && $record->isPlanned() && $record->responsible != bab_getUserId()) {
//                 continue;
//             }
            
            if ($checked) {
                $record->setCompletion(100);
                $this->addMessage($App->translate('The task has been marked as done.'));
            } else {
                $record->setCompletion(0);
            }
        }
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        return true;
    }
    
    
    /**
     * @return \Widget_Displayable_Interface
     */
    public function timeline($select = null)
    {
        $App = $this->App();
//         $Access = $Crm->Access();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        
        $W = bab_Widgets();
        $Ui = $App->Ui();
        $Ui->includeContact();
        
        $timeline = $W->Timeline();
        $timeline->setAutoWidth(true);
        
        $timeline->addBand(new \Widget_TimelineBand('5%', \Widget_TimelineBand::INTERVAL_DAY, 45, \Widget_TimelineBand::TYPE_OVERVIEW));
        $timeline->addBand(new \Widget_TimelineBand('80%', \Widget_TimelineBand::INTERVAL_DAY, 45, \Widget_TimelineBand::TYPE_ORIGINAL));
        $timeline->addBand(new \Widget_TimelineBand('15%', \Widget_TimelineBand::INTERVAL_MONTH, 150, \Widget_TimelineBand::TYPE_OVERVIEW));
        
        
        $now = \BAB_DateTime::now();
        $nowIso = $now->getIsoDate();
        
        $taskStart = $now->cloneDate();
        $taskStart->add(-1, BAB_DATETIME_YEAR);
        
        $taskSet = $this->getRecordSet();
        $taskSelection = $taskSet->dueDate->greaterThan($taskStart->getIsoDate());
        
        switch ($select) {
            case 'user':
                $taskSelection = $taskSelection->_AND_($taskSet->responsible->is($GLOBALS['BAB_SESS_USERID']));
                break;
                
            case 'all':
            default:
                break;
        }
        
        $tasks = $taskSet->select($taskSelection);
        
        $widgetAddonInfo = bab_getAddonInfosInstance('widgets');
        $baseIconPath = $GLOBALS['babUrl'] . $widgetAddonInfo->getTemplatePath() . 'timeline/timeline_js/images/';
        $myNormalIcon = $baseIconPath . 'dark-blue-circle.png';
        $otherNormalIcon = $baseIconPath . 'dull-blue-circle.png';
        $myOverdueIcon = $baseIconPath . 'dark-red-circle.png';
        $otherOverdueIcon = $baseIconPath . 'dull-red-circle.png';
        $myCompleteIcon = $baseIconPath . 'dark-green-circle.png';
        $otherCompleteIcon = $baseIconPath . 'dull-green-circle.png';
        
        $taskCtrl = $App->Task()->Controller();
        
        foreach ($tasks as $task) {
            /* @var $task Task */
            $start = \BAB_DateTime::fromIsoDateTime($task->dueDate);
            $period = $timeline->createMilestone($start);
            $period->start = $task->dueDate . ' 12:00';
            $period->setTitle($task->summary);
            $period->setBubbleContent(
                $W->VBoxItems(
                    $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $taskCtrl->display($task->id)->url()),
                    $W->Html($App->translate('Responsible:') . ' <b>' . bab_toHtml(bab_getUserName($task->responsible, true)) . '</b>'),
                    $W->Html($task->description),
                    $taskActions = $W->FlowItems(
                        $W->Link($App->translate('Details...'), $taskCtrl->display($task->id))
                        ->addClass('widget-actionbutton')
                    )
                    ->setHorizontalSpacing(1, 'em')
                )->setVerticalSpacing(0.5, 'em')
            );
            
            
//             if ($Access->updateTask($task)) {
//                 $taskActions->addItem(
//                     $W->Link($Crm->translate('Edit...'), $Crm->Controller()->Task()->edit($task->id))
//                     ->addClass('widget-actionbutton')
//                     );
//             }
            
            
            if ($task->responsible == $App->getCurrentUser()) {
                $period->classname = 'app-personal-task';
                if ($task->completion >= 100) {
                    $period->icon = $myCompleteIcon;
                } else  {
                    if ($nowIso > $task->dueDate) {
                        $period->icon = $myOverdueIcon;
                    } else {
                        $period->icon = $myNormalIcon;
                    }
                    $period->setTitle($task->summary);
                }
            } else {
                $period->classname = 'app-other-task';
                if ($task->completion >= 100) {
                    $period->icon = $otherCompleteIcon;
                } else  {
                    if ($nowIso > $task->dueDate) {
                        $period->icon = $otherOverdueIcon;
                    } else {
                        $period->icon = $otherNormalIcon;
                    }
                    $period->setTitle($task->summary);
                }
            }
            
            $timeline->addPeriod($period);
        }
        
        return $timeline;
    }
    
    
    
    /**
     *
     * @param string $start			ISO formatted datetime
     * @param string $end			ISO formatted datetime
     */
    public function ical($start = null, $end = null)
    {
        $App = $this->App();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';
        
        
        if (!isset($start)) {
            $start = \bab_DateTime::now();
            $start->add(-1, BAB_DATETIME_MONTH);
            $start = $start->getIsoDateTime();
            $end = \bab_DateTime::now();
            $end->add(6, BAB_DATETIME_MONTH);
            $end = $end->getIsoDateTime();
        }
        
        $caldav = \bab_functionality::get('CalendarBackend/Caldav');
        
        $caldav->includeCalendarPeriod();
        
        $taskSet = $this->getRecordSet();
        $tasks = $taskSet->select(
            $taskSet->scheduledStart->lessThanOrEqual($end)
            ->_AND_($taskSet->scheduledFinish->greaterThanOrEqual($start))
        );
        
        $icalEvents = array();
        foreach ($tasks as $task) {
            $start = \BAB_DateTime::fromIsoDateTime($task->scheduledStart);
            $end = \BAB_DateTime::fromIsoDateTime($task->scheduledFinish);
            
            $calendarPeriod = new \bab_CalendarPeriod();
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('SUMMARY', $task->summary);
            //	        $calendarPeriod->setProperty('DESCRIPTION', $reservation->longDescription);
            
            $icalEvents[] = \caldav_CalendarPeriod::toIcal($calendarPeriod);
        }
        
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: inline; filename=calendar.ics');
        
        echo 'BEGIN:VCALENDAR' . "\r\n"
        . 'VERSION:2.0' . "\r\n"
        . $caldav->getProdId() . "\r\n"
        . $caldav->getTimeZone()
        . implode("\r\n", $icalEvents) . "\r\n"
        . 'END:VCALENDAR';
        
        die;
    }
    
    
    public function userTasks($itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $taskFrame = $App->Task()->Ui()->UserTasks();
        $taskFrame->setUser($App->currentUser());
        $box = $W->VBoxItems(
            $taskFrame
        );
        
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $box->setReloadAction($this->proxy()->userTasks($box->getId()));
        
        return $box;
    }
    
    
    /**
     * Does nothing and return to the previous page.
     *
     * @param array	$message
     */
    public function cancel()
    {
        return true;
    }
    
    
    /**
     * Displays help on this page.
     *
     * @return \Widget_Action
     */
    public function help()
    {
        
    }
    
    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::displayList()
     */
    public function displayList($filter = null, $type = null)
    {
        $App = $this->App();
        $page = $App->Ui()->Page();;
        $page->setTitle($App->translate("Task following"));
        
        if(method_exists($App, "ContactSet")){
            $ContactSet = $App->ContactSet();
            $contact = $ContactSet->get($ContactSet->user->is(bab_getUserId()));
            if($contact){
                $filter['attendees_ID'] = $contact->id;
                $filter["attendees"] = $contact->getFullName();
            }
        }
        $filter["completion_state"] = array("started","created");
        $page->addItem($this->filteredView($filter, $type));
        
        return $page;
    }
    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return \app_TableModelView
     */
    protected function modelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        if($currentComponent = $App->getCurrentComponent()){
            $Ui = $currentComponent->ui();
        }
        else{
            $Ui = $App->Ui();
        }
        
        $recordSet = $this->getRecordSet();
        
        $recordClassname = $this->getRecordClassName();
        
        $itemId = $this->getModelViewDefaultId($itemId);
        
        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }
        
        //$types = $this->getAvailableModelViewTypes();
        
        switch ($type) {
            case 'cards':
                $tableviewClassname =  $currentComponent ? 'cardsView' : $recordClassname . 'CardsView';
                break;
                
            case 'map':
                $tableviewClassname =  $currentComponent ? 'mapView' : $recordClassname . 'MapView';
                break;
                
            case 'calendar':
                $tableviewClassname =  $currentComponent ? 'calendarView' : $recordClassname . 'CalendarView';
                break;
                
            case 'table':
            default:
                $tableviewClassname =  $currentComponent ? 'tableView' : $recordClassname . 'TableView';
                break;
        }
        
        
        /* @var $tableview \widget_TableModelView */
        $tableview = $Ui->$tableviewClassname();
        
        $tableview->setRecordController($this);
        
        $tableview->setId($itemId);
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        $filter = $tableview->getFilterValues();
        
        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }
        
        $conditions = $tableview->getFilterCriteria($filter);
        
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );
        
        $tableview->setDefaultSortField('dueDate:down');

//         $sortfield = $tableview->getSortField();
        $dueDateSorted = false;
//         if($sortfield == 'dueDate:down'){
           $newCond = $conditions->_AND_($recordSet->dueDate->isNot('0000-00-00'));
           $dueDateSorted = true;
           $records = $recordSet->select($newCond);
           $records->orderDesc($recordSet->dueDate);
           $records->orderAsc($recordSet->createdOn);
//         }elseif($sortfield == 'dueDate:up'){
//             $newCond = $conditions->_AND_($recordSet->dueDate->isNot('0000-00-00'));
//             $dueDateSorted = true;
//             $records = $recordSet->select($newCond);
//            $records->orderAsc($recordSet->dueDate);
//            $records->orderDesc($recordSet->createdOn);
//         }
        if($dueDateSorted){
            $zeroRecords = $recordSet->select($conditions->_AND_($recordSet->dueDate->is('0000-00-00')));
        }
        else{
            $records = $recordSet->select($conditions);
        }
        
        
        
//         print_r($records->getSelectQuery());
        
        $tableview->setDataSource($records);
        
        
        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();
            
            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }
        
        $tableview->allowColumnSelection();
        
        return $tableview;
    }
    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function filteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $resetPage = isset($filter['resetPage']);
        unset($filter['resetPage']);
        
        $view = $this->modelView($filter, $type, null, $itemId);
        $view->setAjaxAction();
        
        $filter = $view->getFilterValues();
        
        $filterForm = $view->getAdvancedFilterForm(null, $filter);
        $filterForm->setValues($filter, array('filter'));
        
        $filterPanel = $view->advancedFilterPanel($filter);
        
        if ($this->getFilterVisibility($itemId)) {
            $filterPanel->addClass('show-filter');
        } else {
            $filterPanel->addClass('hide-filter');
        }
        $toolbar = $this->toolbar($view);
        
        
        if ($view->isColumnSelectionAllowed()) {
            $columnSelectionMenu = $view->columnSelectionMenu($view->getVisibleColumns());
            $toolbar->addItem($columnSelectionMenu);
        }
        
        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel,
            $this->previewHighlightedRecords('preview')
            ->setSizePolicy('widget-fixed-right')
            );
        
        $box->setReloadAction($this->proxy()->filteredView(null, null, $view->getId()));
        
        return $box;
    }
    
    /**
     * @param array	$filter
     * @param string $type
     * @return \Widget_Page
     */
    public function displayTypeList($filter = null, $type = null)
    {
        $W = bab_Widgets();
        
        $page = $W->BabPage();
        $page->addClass('app-page-list');
        
        $itemMenus = $this->getListItemMenus();
        
        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
        
        $filteredView = $this->typeFilteredView($filter, $type);
        
        $page->addItem($filteredView);
        
        return $page;
    }
    
    /**
     * @param array     $filter
     * @param string    $type
     * @param string    $itemId
     */
    public function typeFilteredView($filter = null, $type = null, $itemId = null)
    {
        $W = bab_Widgets();
        
        $view = $this->typeModelView($filter, $type, null, $itemId);
        $view->setAjaxAction();
        
        $filter = $view->getFilterValues();
        
        $filterPanel = $view->advancedFilterPanel($filter);
        
        if ($this->getFilterVisibility($itemId)) {
            $filterPanel->addClass('show-filter');
        } else {
            $filterPanel->addClass('hide-filter');
        }
        $toolbar = $this->typeToolbar($view);
        
        $box = $W->VBoxItems(
            $toolbar,
            $filterPanel
        );
        $box->setReloadAction($this->proxy()->typeFilteredView(null, null, $view->getId()));
        
        return $box;
    }
    
    public function setTableviewFilter($filter)
    {
        $modelView = $this->modelView(null, 'table')->setFilterValues($filter);
        $this->addReloadSelector('.depends-'.$modelView->getId());
        return true;
    }
    
    protected function toolbar($tableView)
    {
        $App = $this->App();
        $W = bab_Widgets();
        $toolbar = parent::toolbar($tableView);
        
        $contactSet = $App->ContactSet();
        $contact = $contactSet->get($contactSet->user->is($App->getCurrentUser()));
        
        if($contact){
            $filter = $tableView->getFilterValues();
            $filter['attendees_ID'] = $contact->getRef();
            $toolbar->addItem(
                $W->Link(
                    $App->translate('My tasks'),
                    $this->proxy()->setTableviewFilter($filter)
                )->addClass('icon', \Func_Icons::ACTIONS_USER_PROPERTIES)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }
        
        $toolbar->addItem(
            $this->selectedItemsToolbar($tableView->getId())
        );
        
        return $toolbar;
    }
    
    public function getModelViewSelectedRows($itemId = null){
        $tableid = $this->getModelViewDefaultId($itemId);
        $inSession = false;
        return bab_Widgets()->getUserConfiguration($tableid . '/selectedRows', 'widgets', $inSession);
    }
    
    /**
     *
     * @param string $modelViewId
     * @param string $itemId
     * @return Widget_FlowLayout
     */
    public function selectedItemsToolbar($modelViewId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $box = $W->FlowItems()
        ->setHorizontalSpacing(1, 'ex');
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        
        $nbSelected = count($this->getModelViewSelectedRows());
        if ($nbSelected > 0) {
            $box->addItem(
                $W->Html(
                    '<b>' . bab_toHtml($App->translate('Selection:')) . '</b>'
                )
            );
            $box->addItem(
                $W->Link(
                    $nbSelected,
                    $this->proxy()->showSelectedRecords()
                )->setTitle($App->translate('Show selected tickets'))
                ->addClass('badge')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
            $box->addItem(
                $W->Link(
                    '',
                    $this->proxy()->clearSelectedRecords()
                    )->addClass('icon', \Func_Icons::ACTIONS_DIALOG_CANCEL)
                ->setTitle($App->translate('Clear selection'))
                ->setAjaxAction()
                );
            $box->addItem(
                $W->Html(
                    '<b>' . bab_toHtml($App->translate('Actions:')) . bab_nbsp() . '</b>'
                )
            );
            
//             $box->addItem(
//                 $W->Link(
//                     $App->translate('Change status'),
//                     $this->proxy()->editMultiStatus()
//                     )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES)
//                 ->setOpenMode(Widget_Link::OPEN_DIALOG)
//                 );
            $box->addItem(
                $W->Link(
                    $App->translate('Add attendee'),
                    $this->proxy()->addMultiAttendee()
                )->addClass('icon', \Func_Icons::ACTIONS_USER_NEW)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
            $box->addItem(
                $W->Link(
                    $App->translate('Mark as done'),
                    $this->proxy()->setMultiDone()
                )->addClass('icon', \Func_Icons::ACTIONS_DIALOG_OK)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
            
            
            $box->addClass('widget-toolbar-multiselect alert-info');
        }
        
        $box->addClass('depends-' . $modelViewId . '-selection');
        $box->setReloadAction($this->proxy()->selectedItemsToolbar($modelViewId, $box->getId()));
        return $box;
    }
    
    public function clearSelectedRecords()
    {
        $modelView = $this->modelView(null, 'table');
        $modelView->clearSelectedRows();
        
        $this->addReloadSelector('.depends-' . $modelView->getId());
        
        return true;
    }
    
    public function clearSelectedRecord($id)
    {
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();
        
        unset($selectedRows[$id]);
        
        $modelView->setSelectedRows($selectedRows);
        
        $this->addReloadSelector('.depends-' . $modelView->getId());
        
        return true;
    }
    
    public function showSelectedRecords()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $modelView = $this->modelView(null, 'table');
        
        $selectedRows = $modelView->getSelectedRows();
        $page = $App->Ui()->Page();
        $page->setTitle($App->translate('Selected tasks'));
        
        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->in($selectedRows));
        
        foreach ($records as $record) {
            $page->addItem(
                $W->FlowItems(
                    $W->Link(
                        $record->summary,
                        $this->proxy()->display($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-50em'),
                    $W->Link(
                        '',
                        $this->proxy()->clearSelectedRecord($record->id)
                    )->setAjaxAction()
                    ->setTitle($App->translate('Remove from selection'))
                    ->addClass('widget-actions', 'icon', \Func_Icons::ACTIONS_DIALOG_CANCEL)
                    ->setSizePolicy(\Func_Icons::ICON_LEFT_16)
                )->setSizePolicy('widget-list-element widget-actions-target')
            );
        }
        
        $page->addClass('depends-' . $modelView->getId());
        $page->setReloadAction($this->proxy()->showSelectedRecords());
        return $page;
    }
    
    protected function typeToolbar($tableView)
    {
        $toolbar = parent::toolbar($tableView);
        
        $W = bab_Widgets();
        $App = $this->App();
        
        $toolbar->addItem(
            $W->FlowItems(
                $W->Link(
                    $App->translate('Edit default blueprint'),
                    $App->Controller()->CustomSection()->editSections('Task', $App->getComponentByName('TaskType')->recordSet()->getDefaultBlueprint())
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $W->Link(
                    $App->translate('Create new type'),
                    $this->proxy()->editType()
                )->addClass('icon', \Func_Icons::ACTIONS_ARTICLE_NEW)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->setHorizontalSpacing(1, 'em')
        );
        
        return $toolbar;
    }
    
    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return TaskTypeTableView
     */
    protected function typeModelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        $component = $App->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        $Ui = $component->ui();
        
        $recordSet = $component->recordSet();
        
        $itemId = $this->getModelViewDefaultId($itemId);
        
        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }
        
        /* @var $tableview TaskTypeTableView */
        $tableview = $Ui->TypeTableView($itemId);
        
        $tableview->setRecordController($this);
        
        $tableview->setId($itemId);
        
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        $filter = $tableview->getFilterValues();
        
        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }
        
        $conditions = $tableview->getFilterCriteria($filter);
        
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );
        
        $records = $recordSet->select($conditions);
        
        $tableview->setDataSource($records);
        
        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();
            
            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }
        
        $tableview->allowColumnSelection();
        
        return $tableview;
    }
    
    /**
     * Save a record
     *
     * @requireSaveMethod
     *
     * @param array $data
     */
    public function saveType($data = null)
    {
        $this->requireSaveMethod();
        $App = $this->App();
        
        $component = $App->getComponentByName('TaskType');
        
        if ($component) {
            $recordSet = $component->recordSet();
        } else {
            throw new \app_SaveException('The TaskType component has not been found');
        }
        
        $pk = $recordSet->getPrimaryKey();
        $message = null;
        $isNewType = false;
        
        if (!empty($data[$pk])) {
            $record = $recordSet->request($data[$pk]);
            unset($data[$pk]);
            
            if (!$record->isUpdatable()) {
                throw new \app_AccessException('Access denied');
            }
            
            $message = $App->translate('The task type has been saved');
        } else {
            
            $record = $recordSet->newRecord();
            
            if (!$recordSet->isCreatable()) {
                throw new \app_AccessException('Access denied');
            }
            
            $message = $App->translate('The task type has been created');
            $isNewType = true;
        }
        
        if (!isset($record)) {
            throw new \app_SaveException($App->translate('The record does not exists'));
        }
        
        
        if (!isset($data)) {
            throw new \app_SaveException($App->translate('Nothing to save'));
        }
        
        $record->setFormInputValues($data);
        
        if ($record->save()) {
            
            if($record->isDefault){
                $allTypes = $recordSet->select($recordSet->id->is($record->id)->_NOT()->_AND_($recordSet->isDefault->is(true)));
                foreach ($allTypes as $type){
                    $type->isDefault = false;
                    $type->save();
                }
            }
            
            if($isNewType){
                $record->setDefaultBlueprint();
            }
            
            if (is_string($message)) {
                $this->addMessage($message);
            }
            
            $this->addReloadSelector('.depends-' . $component->getRecordClassName());
            return true;
        }
        
        return true;
    }
    
    public function editType($taskType = null)
    {
        $App = $this->App();
        
        $component = $App->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $recordSet = $component->recordSet();
        $taskType = $recordSet->get($taskType);
        
        $page->setTitle($App->translate('Edit task'));
        
        $editor = $component->Ui()->typeEditor();
        
        $editor->setName('data');
        $editor->isAjax = bab_isAjaxRequest();
        if($taskType){
            $editor->setTaskType($taskType);
        }
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function comfirmDeleteTaskType($id = null)
    {
        $App = $this->App();
        
        $component = $App->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        
        $W = bab_Widgets();
        $Ui = $App->Ui();
        
        $recordSet = $component->recordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($App->translate('This element does not exists.'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($App->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($App->translate('It will not be possible to undo this deletion.'))));
        
        $confirmedAction = $this->proxy()->deleteTaskType($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($App->translate('Delete'))
        );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function deleteTaskType($id)
    {
        $this->requireDeleteMethod();
        
        $App = $this->App();
        
        $component = $App->getComponentByName('TaskType');
        if(!$component){
            throw new \app_Exception('The TaskType component has not been found');
        }
        
        $recordSet = $component->recordSet();
        
        $record = $recordSet->request($id);
        
        if (!$record->isDeletable()) {
            throw new \app_AccessException('Sorry, You are not allowed to perform this operation');
        }
        $deletedMessage = $App->translate('The task type has been deleted');
        
        if ($record->delete()) {
            $this->addMessage($deletedMessage);
        }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        
        return true;
    }
    
    /**
     * @param string $for       Record reference
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function listFor($for, $type = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems()->setIconFormat(16, 'left');
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        else{
            $_SESSION['componentTask_displayCompletedTasks'] = true;
        }
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        $recordSet = $this->getRecordSet();
        
        $conditions = array();
        $conditions[] = $recordSet->isTargetOf($forRecord);
        if(isset($type) && $App->getComponentByName('TaskType')){
            $conditions[] = $recordSet->type->is($type);
        }
        if(!isset($_SESSION['componentTask_displayCompletedTasks']) || !$_SESSION['componentTask_displayCompletedTasks']){
            $conditions[] = $recordSet->isNotCompleted();
            $toggleLabel = $App->translate('Show completed tasks');
            $icon = 'objects-eye';
        }
        else{
            $toggleLabel = $App->translate('Hide completed tasks');
            $icon = 'objects-eye-slash';
        }
        
        $box->addItem(
            $W->Link($toggleLabel, $this->proxy()->toggleCompletedTasksDisplay())->setOpenMode(\Widget_Link::OPEN_DIALOG)->setIcon($icon)
        );
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderAsc($recordSet->dueDate);
        $records->orderDesc($recordSet->modifiedOn);
        
        $listElement = $this->newListItem($for, $type);
        $box->addItem(
            $listElement->setSizePolicy('widget-list-element')
        );
        
        $nbRecords = $records->count();
        $nbItems = 0;
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listFor($for, $type, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        
        $box->setReloadAction($this->proxy()->listFor($for, $type, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    public function toggleCompletedTasksDisplay()
    {
        if(!isset($_SESSION['componentTask_displayCompletedTasks'])){
            $_SESSION['componentTask_displayCompletedTasks'] = true;
        }
        else{
            $_SESSION['componentTask_displayCompletedTasks'] = !$_SESSION['componentTask_displayCompletedTasks'];
        }
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        return true;
    }
    
    /**
     * @param string $for       Record reference
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function milestonesListFor($for, $type = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems()->setSizePolicy('widget-actions-target widget-list-element');
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        $recordSet = $this->getRecordSet();
        
        $conditions = array();
        $conditions[] = $recordSet->isTargetOf($forRecord);
        $conditions[] = $recordSet->milestone->is(true);
        if(isset($type) && $App->getComponentByName('TaskType')){
            $conditions[] = $recordSet->type->is($type);
        }
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderDesc($recordSet->modifiedOn);
        
        $nbRecords = $records->count();
        $nbItems = 0;
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->milestoneListFor($for, $type, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        
        $box->setReloadAction($this->proxy()->milestoneListFor($for, $type, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    /**
     * @param string $for       Record reference
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function nextMilestonesListFor($for, $type = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        /**
         * @var TaskSet $recordSet
         */
        $recordSet = $this->getRecordSet();
        
        $now = \BAB_DateTime::now();
        $now->setTime(0, 0, 0);
        
        $conditions = array();
        $conditions[] = $recordSet->isTargetOf($forRecord);
        $conditions[] = $recordSet->milestone->is(true);
        $conditions[] = $recordSet->dueDate->greaterThanOrEqual($now->getIsoDate())->_OR_($recordSet->completion->is(100)->_NOT());
        if(isset($type) && $App->getComponentByName('TaskType')){
            $conditions[] = $recordSet->type->is($type);
        }
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderDesc($recordSet->modifiedOn);
        
        $nbRecords = $records->count();
        
        $nbItems = 0;
        foreach ($records as $record) {
            $isLate = \BAB_DateTime::compare($now, \BAB_DateTime::fromIsoDateTime($record->dueDate)) == 1 && !$record->isCompleted();    
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->nextMilestonesListFor($for, $type, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            if($isLate){
                $listElement->setSizePolicy($listElement->getSizePolicy().' alert-danger');
            }
            $box->addItem($listElement);
        }
        
        if($nbRecords != 0 && $nbItems >= $nbRecords){
            $box->setSizePolicy('widget-actions-target widget-list-element');
        }
        
        $box->setReloadAction($this->proxy()->nextMilestonesListFor($for, $type, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    
    
    
    
    /**
     * @param string $contact       Contact id
     * @param string $participantType
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function listForParticipant($contact, $participantType = null, $taskType = null, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems()->setSizePolicy('widget-actions-target widget-list-element');
        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $contactSet = $App->ContactSet();
        $contact = $contactSet->request($contact);
        
        $recordSet = $this->getRecordSet();
        
        $conditions = array();
        $conditions[] = $recordSet->hasAttendee($contact, $participantType);
        $conditions[] = $recordSet->isNotCompleted();
        
        if (isset($taskType)) {
            $conditions[] = $recordSet->type->is($taskType);
        }
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderAsc($recordSet->dueDate);
        
        $nbRecords = $records->count();
        $nbItems = 0;
        
        $listElement = $this->newListItem();
        $box->addItem(
            $listElement->setSizePolicy('widget-list-element')
        );
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listForParticipant($contact->id, $participantType, $taskType, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record, true);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        
        $box->setReloadAction($this->proxy()->listForParticipant($contact->id, $participantType, $taskType, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    
    /**
     * Displays a page asking to confirm the completion of a task
     *
     * @param int $id
     */
    public function confirmDone($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Completion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($App->translate('This element does not exists.'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $App->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($App->translate('Mark this task as done?'), 6));
        
        $confirmedAction = $this->proxy()->markDone($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($App->translate('Mark as done'))
        );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    /**
     * Displays a page asking to confirm the deletetion of a task link
     *
     * @param int $id
     */
    public function confirmDeleteTaskLink($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $recordSet = $App->LinkSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($App->translate('Remove link to element'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($App->translate('This link does not exists.'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $form->addItem($W->Title($App->translate('Remove this element link?'), 6));
        
        $confirmedAction = $this->proxy()->deleteTaskLink($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAjaxAction($confirmedAction)
            ->setLabel($App->translate('Delete link'))
        );
        $form->addButton($W->SubmitButton()->setLabel($App->translate('Cancel'))->addClass('widget-close-dialog'));
        $page->addItem($form);
        
        return $page;
    }
    
    public function deleteTaskLink($id)
    {
        $this->requireDeleteMethod();
        
        $App = $this->App();
        $recordSet = $App->LinkSet();
        
        $record = $recordSet->request($id);
        
        $record->delete();
//         if (!$record->isDeletable()) {
//             throw new \app_AccessException('Sorry, You are not allowed to perform this operation');
//         }
//         $deletedMessage = $App->translate('The link has been deleted');
        
//         if ($record->delete()) {
//             $this->addMessage($deletedMessage);
//         }
        
        $this->addReloadSelector('.depends-' . $record->getClassName());
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        
        return true;
    }
    
    
    
    public function newListItem($for = null, $type = null, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        if(!isset($type)){
            if($taskTypeComp = $App->getComponentByName('TaskType')){
                /* @var $set TaskTypeSet */
                $set = $taskTypeComp->recordSet();
                $defaultType = $set->get($set->isDefault->is(true));
                if($defaultType){
                    $type = $defaultType->id;
                }
            }
        }
        
        $editor = new \app_Editor($App, $itemId);
        $editor->setName('data');
        $editor->addClass('depends-' . $this->getRecordClassName());
        
        $line = $W->HBoxItems();
        $line->setVerticalAlign('middle')
            ->setHorizontalSpacing(1, 'em')
            ->addClass('widget-100pc', \Func_Icons::ICON_LEFT_16);

        $summaryBox = $W->LineEdit()
            ->setName('summary')
            ->setPlaceHolder($App->translate('Enter a new task'));

        $line->addItem(
            $summaryBox->setSizePolicy('maximum')
        );

        $line->setIconFormat(16, 'left');
        
        $contactSet = $App->ContactSet();
        $currentContact = $contactSet->get($contactSet->user->is(bab_getUserId()));
        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->addItem($line);
        
        $values = array(
            'summary' => ''
        );
        
        if(isset($currentContact)){
            $editor->addItem($W->Hidden(null, 'newAttendee_ID'));
            $values['newAttendee_ID'] = $currentContact->getRef();
        }
        if (isset($for)) {
            $editor->addItem($W->Hidden(null, 'for'));
            $values['for'] = $for;
        }
        if (isset($type)) {
            $editor->addItem($W->Hidden(null, 'type'));
            $values['type'] = $type;
        }
        
        //Upon editor load (first time or after a submit), we reset the form values to its origin
        $summaryLineEditId = $summaryBox->getId();
        $editor->addItem(
            $W->Html(
            '<script>
                jQuery("#'.$summaryLineEditId.'").ready(() => {
                    var formId = "#"+jQuery("#'.$summaryLineEditId.'").attr("form");
                    var form = jQuery(formId);
                    if(form.length > 0){
                        var observer = new MutationObserver(function(mutations) {
                          mutations.forEach(function(mutation) {
                            if (mutation.attributeName === "class") {
                              form[0].reset();
                            }
                          });
                        });
                        observer.observe(form[0], {
                          attributes: true
                        });
                    }
                })
            </script>'    
            )
        );
        
        $editor->setValues($values, array('data'));
        $editor->setAjaxAction($this->proxy()->save(), false, 'keypress:13');
        $editor->setReloadAction($this->proxy()->newListItem($for, $type, $editor->getId()));
        
        return $editor;
    }
    
    
    
    public function saveNewTask($data = null)
    {
        $set = $this->getRecordSet();
        
        $record = $set->newRecord();
        $record->setFormInputValues($data);
        $record->save();
        
        return true;    
    }
    
    
    public function listItem($id, $withObjects = false)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        if ($id instanceof \app_Record) {
            $record = $id;
        } else {
            $record = $this->getRecordSet()->request($id);
        }
        
        $attendeesBox = $W->FlowItems();
        $attendeesBox->setSizePolicy('widget-2em');
        $taskAttendees = $record->getAttendees();
        foreach ($taskAttendees as $taskAttendee) {
            $attendeeRecord = $App->getRecordByRef($taskAttendee->attendee);
            if (! ($attendeeRecord instanceof \Isi4You_Contact)) {
                continue;
            }
            $attendeesBox->addItem(
                $W->Link(
                    $Ui->ContactAvatar($attendeeRecord, 20, 20),
                    $App->Controller()->Contact()->display($attendeeRecord->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($attendeeRecord->getFullName())
            );
        }
        
        $taskCtrl = $App->Task()->Controller();
        
        $line = $W->HBoxItems();
        $line->setVerticalAlign('middle')
            ->setHorizontalSpacing(1, 'em')
            ->addClass('widget-100pc', \Func_Icons::ICON_LEFT_16);
        
        if($record->isUpdatable()) {
            $form = new \app_Editor($App);
            $form->setName('data');
            $form->isAjax = true;
            $form->addClass('widget-no-close');
            $form->addItem(
                $W->Hidden(null, 'id', $record->id)    
            );
            $form->addItem(
                $W->CheckBox()->setName('completion')->setValue($record->isCompleted())
            );
            
            $form->setAjaxAction($this->proxy()->changeTaskCompletionStatus(), '', 'change');
            $line->addItem($form);
        }
        else{
            $line->addItem($W->CheckBox()->disable());
        }
        
        $summaryBox = $W->Link(
            $record->summary,
            $taskCtrl->edit($record->id)
        )->setOpenMode(\Widget_Link::OPEN_DIALOG)
        ->setTitle($record->summary)
        ->addClass('widget-ellipsis');
        
        if ($withObjects) {
            $summaryBox = $W->VBoxItems($summaryBox);
            $objects = $record->getLinkedObjects();
            foreach ($objects as $object) {
                /* @var $object \app_Record */
                if(!$object){
                    continue;
                }
                $ctrl = $object->getController();
                if (method_exists($ctrl, 'display')) {
                    $icon = '';
                    if ($object instanceof \Isi4You_Contact) {
                        $icon = 'objects-contact';
                    } elseif  ($object instanceof \Isi4You_Organization) {
                        $icon = 'objects-organization';
                    } elseif ($object instanceof \Isi4You_Deal) {
                        $icon = 'objects-contract';
                    }
                    $summaryBox->addItem(
                        $W->Link(
                            $object->getRecordTitle(),
                            $object->getController()->display($object->id)
                        )->addClass('widget-small', 'icon', $icon)
                    );
                }
            }
        }
        
        $line->addItem(
            $summaryBox->setSizePolicy('widget-50pc')
        );
        $line->addItem(
            $W->Label('')->addClass($record->milestone ? 'icon objects-flag' : 'icon')
        );
        
        $line->addItem(
            $W->Label(bab_shortDate(bab_mktime($record->dueDate), false))
                ->addClass($record->private ? 'icon ' . \Func_Icons::ACTIONS_AUTHENTICATE : '')
                ->setSizePolicy('widget-30pc widget-align-right')
        );
        $line->addItem(
            $attendeesBox->setSizePolicy('widget-15pc widget-align-middle')
        );
        
        
        $actions = $W->FlowItems()->addClass('widget-actions');
        
        if ($record->isDeletable()) {
            $actions->addItem(
                $W->Link(
                    '',
                    $taskCtrl->confirmDelete($record->id)
                )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
            );
        }
        
        $line->addItem(
            $actions->setSizePolicy('minimum')
        );
        
        
        if ($record->isLate()) {
            $line->addClass('alert-danger');
        }
//         $line->setSizePolicy('widget-list-element');
            
        $line->setIconFormat(16, 'left');
        
        return $line;
    }
    
    public function changeTaskCompletionStatus($data = null)
    {
        $this->requireSaveMethod();
        
        if(!isset($data['id']) || empty($data['id']) || !isset($data['completion'])){
            return true;
        }
        
        $set = $this->getRecordSet();
        $record = $set->get($set->id->is($data['id']));
        
        if($record){
            $completion = $data['completion'] ? 100 : 0;
            $record->setCompletion($completion);
            $record->save();
            
            $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        }
        
        return true;
    }
    
    /**
     * @param string $for       Record reference
     * @param int $limit
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function listPendingFor($for, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        $recordSet = $this->getRecordSet();
        
        $conditions = array(
            $recordSet->isTargetOf($forRecord),
            $recordSet->isNotCompleted()
        );
        
        $records = $recordSet->select(
            $recordSet->all($conditions)
        );
        $records->orderAsc($recordSet->dueDate);
        $records->orderDesc($recordSet->modifiedOn);
        
        $listElement = $this->newListItem($for);
        $box->addItem(
            $listElement->setSizePolicy('widget-list-element')
        );
        $nbRecords = $records->count();
        $nbItems = 0;
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listPendingFor($for, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        
        $box->setReloadAction($this->proxy()->listPendingFor($for, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    
    
    /**
     * @param string $for       Record reference
     * @param int $limit
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function listCompletedFor($for, $limit = 5, $itemId = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $box = $W->VBoxItems();
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $forRecord = $App->getRecordByRef($for);
        if (!isset($forRecord)) {
            return $box;
        }
        
        $recordSet = $this->getRecordSet();
        
        $records = $recordSet->select(
            $recordSet->all(
                $recordSet->isTargetOf($forRecord),
                $recordSet->isCompleted()
            )
        );
        $records->orderAsc($recordSet->dueDate);
        $records->orderDesc($recordSet->modifiedOn);
        
        $nbRecords = $records->count();
        $nbItems = 0;
        foreach ($records as $record) {
            if ($nbItems >= $limit) {
                $box->addItem(
                    $W->Link(
                        sprintf($App->translate('Show all (%d more)'), $nbRecords - $nbItems),
                        $this->proxy()->listCompletedFor($for, 100)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->setSizePolicy('widget-align-center')
                    ->addClass('widget-actionbutton')
                );
                break;
            }
            $nbItems++;
            $listElement = $this->listItem($record);
            $box->addItem(
                $listElement->setSizePolicy('widget-list-element')
            );
        }
        
        $box->setReloadAction($this->proxy()->listCompletedFor($for, $limit, $box->getId()));
        $box->addClass('depends-' . $this->getRecordClassName());
        return $box;
    }
    
    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestParticipant($task = null, $search = null)
    {
        $App = $this->App();
        $Ui = $App->Task()->Ui();
        
        bab_requireCredential($App->translate('You must be logged'), 'Basic');
        
        $suggest = $Ui->SuggestParticipant();
        $suggest->setTask($task);
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();
        
        die;
    }
    
    public function getAvailableDisplayFields()
    {
        $availableFields = parent::getAvailableDisplayFields();
        
        unset($availableFields['uuid']);
        unset($availableFields['id']);
        unset($availableFields['send']);
        unset($availableFields['canceledAction']);
        unset($availableFields['canceledOn']);
        unset($availableFields['completedAction']);
        unset($availableFields['completedBy']);
        unset($availableFields['completedOn']);
        unset($availableFields['createdBy']);
        unset($availableFields['createdOn']);
        unset($availableFields['deleted']);
        unset($availableFields['deletedBy']);
        unset($availableFields['deletedOn']);
        unset($availableFields['modifiedBy']);
        unset($availableFields['modifiedOn']);
        unset($availableFields['parent']);
        unset($availableFields['sortkey']);
        unset($availableFields['scheduledAllDay']);
        unset($availableFields['scheduledStart']);
        unset($availableFields['scheduledStartTime']);
        unset($availableFields['scheduledFinish']);
        unset($availableFields['scheduledFinishTime']);
        unset($availableFields['startedAction']);
        unset($availableFields['startedOn']);
        
        $availableFields['alarms'] = array(
            'name' => 'alarms',
            'description' => 'alarms'
        );
        
        return $availableFields;
    }
    
    public function setMultiDone()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();
        
        $selectedRows = $this->getModelViewSelectedRows();
        $Ui->includeTicket();
        
        $nbSelected = count($selectedRows);
        
        $page = $Ui->Page();
        $page->setTitle(sprintf($App->translate('Mark as done for %d task', 'Mark as done for %d tasks', $nbSelected), $nbSelected));
        
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setName('data');
        
        $editor->addItem(
            $W->Label(sprintf($App->translate('The selected task will be marked as done', 'The %d selected tasks will be marked as done', $nbSelected), $nbSelected))    
        );
        
        $editor->setSaveAction($this->proxy()->saveMultiDone());
        $editor->isAjax = true;
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveMultiDone($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->TaskSet();
        $selectedRows = $this->getModelViewSelectedRows();
   
        $recordSet = $this->getRecordSet();
        
        foreach($selectedRows as $selectId){
            $record = $recordSet->get($selectId);
            if($record){
                $record->setCompletion(100);
            }
        }
        
        $this->addReloadSelector('.depends-Task');
        
        return true;
    }
    
    /**
     * @param int           $id                   Task id
     * @param string|null   $itemId
     * @return \Widget_FlowLayout
     */
    public function _dueDate($id, $itemId = null)
    {
        $W = bab_Widgets();
        
        $box = $W->FlowItems()->addClass(\Func_Icons::ICON_LEFT_16);
        
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        
        $recordSet = $this->getRecordSet(true);
        $record = $recordSet->request($id);
        
        $label = $W->Label(bab_shortDate(bab_mktime($record->dueDate), false))->setIcon(\Func_Icons::APPS_CALENDAR);
        
        if($record->isUpdatable()){
            $box->addItems(
                $label,
                $W->HBoxItems(
                    $W->Link(
                        '',
                        $this->proxy()->editDueDate($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                    ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT)
                )->addClass('widget-actions')
            )->setVerticalAlign('middle')
            ->addClass('widget-actions-target');
        }
        else{
            $box->addItem($label);
        }
        
        $box->setReloadAction($this->proxy()->_dueDate($record->id, $box->getId()));
        $box->addClass('depends-task_'.$record->id);
        return $box;
    }
    
    public function editDueDate($id)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $recordSet = $this->getRecordSet(true);
        $record = $recordSet->request($id);
        
        $page = $Ui->Page();
        $page->setTitle($App->translate('Edit due date'));
        
        $editor = $Ui->Editor();
        $editor->setName('data');
        $editor->addItem($W->Hidden(null, 'id'));
        
        $box = $W->HBoxItems()->setHorizontalAlign("widget-center")->setHorizontalSpacing(1,"em");

        $box->addItem(
            $W->DatePicker()->setName('dueDate')
        );
        
        $editor->addItem($box);
        $editor->setRecord($record);
        
        $editor->isAjax = true;
        $editor->setSaveAction($this->proxy()->saveDueDate());
        
        $page->addItem($editor);
        
        
        return $page;
    }
    
    public function saveDueDate($data = null)
    {
        $this->requireSaveMethod();
        
        $recordSet = $this->getRecordSet(true);
        $record = $recordSet->request($data['id']);
        
        unset($data['id']);
        
        $record->setFormInputValues($data);
        if($record->save()){
            $this->addReloadSelector('.depends-task_'.$record->id);
        }
        return true;
    }
}